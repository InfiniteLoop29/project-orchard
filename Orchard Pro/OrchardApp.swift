//
//  OrchardApp.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-09-25. Credit to Omar for testing! Although you wont ever see this... 3,398 lines
//

import SwiftUI

@main
struct OrchardApp: App {
    #if os(macOS)
        class AppDelegate: NSObject, NSApplicationDelegate {
            private var aboutBoxWindowController: NSWindowController?

            func showAboutPanel() {
                if aboutBoxWindowController == nil {
                    let styleMask: NSWindow.StyleMask = [.closable, .miniaturizable, .titled]
                    let window = NSWindow()
                    window.styleMask = styleMask
                    window.title = "About Orchard Pro"
                    window.contentView = NSHostingView(rootView: AboutAppView())
                    aboutBoxWindowController = NSWindowController(window: window)
                }

                aboutBoxWindowController?.showWindow(aboutBoxWindowController?.window)
            }
        }

        @NSApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    #endif
    @StateObject private var modelData = ModelData()
    @State private var filter = 1
    var body: some Scene {
        WindowGroup {
            if modelData.appSettings.colorScheme == 0 {
            ContentView()
                .colorScheme(.light)
            } else   if modelData.appSettings.colorScheme == 1 {
                ContentView()
                    .colorScheme(.dark)
                } else   if modelData.appSettings.colorScheme == 2 {
                    ContentView()
                  
                    }
        }
        #if os(macOS)
        .commands {
            SidebarCommands()

            CommandGroup(replacing: CommandGroupPlacement.appInfo) {
                Button(action: {
                    appDelegate.showAboutPanel()

                }) {
                    Text("About Orchard Pro")
                }                  .keyboardShortcut("a", modifiers: [.control])
            }
        }
        #endif
        #if os(macOS)
            Settings {
                SettingsView()
            }
        #endif
    }
}
