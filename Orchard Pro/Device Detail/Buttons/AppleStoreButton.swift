//
//  AppleStoreButton.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-12-07.
//

import SwiftUI

struct AppleStoreButton: View {
    var orchard: Orchard
    @StateObject var modelData = ModelData()
    @Environment(\.openURL) var openURL

    var body: some View {
#if os(iOS)
        Link(destination: URL(string: "\(orchard.URLScheme)")!) {
          
            Label("applestorebutton_title", systemImage: "bag")
        }
        #else
        Button {
            openURL(URL(string: "\(orchard.URLScheme)")!)
               } label: {
                   Image(systemName: "safari")
               }
        #endif
    }
}

struct AppleStoreButton_Previews: PreviewProvider {
    static let modelData = ModelData()
    static var previews: some View {
        AppleStoreButton(orchard: modelData.orchardData[0])
    }
}
