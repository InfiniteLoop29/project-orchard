//
//  FavoriteButton.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-10-31.
//

import SwiftUI

struct FavoriteButton: View {
    @Binding var isSet: Bool

    var body: some View {
        Button(action: {
            #if os(iOS)
                playSelectionHaptic()
            #endif
            isSet.toggle()
        }) {
            Image(systemName: isSet ? "star.fill" : "star")
            #if os(iOS)
                .foregroundColor(isSet ? Color.yellow : Color.gray)
            #endif
                .font(.title)
        }
    }
}

struct FavoriteButton_Previews: PreviewProvider {
    static var previews: some View {
        FavoriteButton(isSet: .constant(true))
    }
}
