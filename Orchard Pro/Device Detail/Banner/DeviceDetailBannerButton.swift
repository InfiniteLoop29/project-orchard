//
//  DeviceCircleView.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-09-25.
//

import SwiftUI

struct DeviceDetailBannerButton: View {
    @State private var isShowingARAlert = false
    @State var showingPreview = false
    var showARBadge: Bool
    var image: String
    var ARFileName: String
    var body: some View {
        if showARBadge {
            #if os(iOS)
                NavigationLink(destination: ARQuickLookView(name: ARFileName).ignoresSafeArea()) {
                    ZStack {
                        Image(image)
                            .resizable()
                            .padding()
                            .frame(width: 180.0, height: 180.0)
                            .background(.thinMaterial, in:
                                RoundedRectangle(cornerRadius: 20))
                            .shadow(radius: 5)
                        if showARBadge {
                            HStack {
                                Image("ARKitBadge")
                                    .resizable()
                                    .frame(width: 70, height: 38)
                            }
                            .offset(x: 46, y: -63)
                        }
                    }
                }
            #endif
        } else {
            Button(action: {
                #if os(iOS)
                    playSelectionHaptic()
                #endif
                isShowingARAlert = true

            }) {
                ZStack {
                    Image(image)
                        .resizable()
                        .padding()
                    
                        .frame(width: 180.0, height: 180.0)
                        .background(.thinMaterial, in:
                            RoundedRectangle(cornerRadius: 20))
                        .shadow(radius: 5)
                }
            }
            #if os(iOS)
            .alert(isPresented: $isShowingARAlert, content: {
                       Alert(title: Text("AR Preview"), message: Text("AR Quick Look is not supported for this device, look for the ARKit symbol"), dismissButton: .default(Text("OK"), action: {
                       }))
                   })
    .buttonStyle(.plain)
            #else
                    .alert(isPresented: $isShowingARAlert, content: {
                               Alert(title: Text("AR Preview"), message: Text("AR Quick Look is not supported on macOS"), dismissButton: .default(Text("OK"), action: {
                               }))
                           })
            .buttonStyle(.plain)
            #endif
        }
    }
}

struct DeviceDetailBannerButton_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            DeviceDetailBannerButton(showARBadge: true, image: "iphone_12", ARFileName: "")
                .previewLayout(.fixed(width: 190, height: 190))
        }
    }
}
