//
//  BannerView.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-12-14.
//

import SwiftUI

struct DetailBannerView: View {
    @State var image: String

    var body: some View {
        Image(image)
            .resizable()
            .overlay(.ultraThinMaterial)
    }
}

struct DetailBannerView_Previews: PreviewProvider {
    static var previews: some View {
        DetailBannerView(image: "iphone_12_wallpaper")
    }
}
