//
//  DeviceInfoView.swift
//  Orchard
//
//  Created by Drake Jordan on 2022-01-23.
//

import SwiftUI
#if os(iOS)
    func playSelectionHaptic() {
        let generator = UISelectionFeedbackGenerator()
        generator.selectionChanged()
    }
#endif
struct DeviceDetailView: View {
    @EnvironmentObject var modelData: ModelData
    var orchard: Orchard

    var orchardIndex: Int {
        modelData.orchardData.firstIndex(where: { $0.id == orchard.id })!
    }

    private func getScrollOffset(_ geometry: GeometryProxy) -> CGFloat {
        geometry.frame(in: .global).minY
    }

    private func getOffsetForHeaderImage(_ geometry: GeometryProxy) -> CGFloat {
        let offset = getScrollOffset(geometry)

        if offset > 0 {
            return -offset
        }

        return 0
    }

    private func getHeightForHeaderImage(_ geometry: GeometryProxy) -> CGFloat {
        let offset = getScrollOffset(geometry)
        let imageHeight = geometry.size.height

        if offset > 0 {
            return imageHeight + offset
        }

        return imageHeight
    }

    var body: some View {
        VStack {
            ScrollView {
                ZStack {
                    GeometryReader { geometry in
                        DetailBannerView(image: orchard.bannerName)
                            .scaledToFill()
                            .frame(width: geometry.size.width, height: self.getHeightForHeaderImage(geometry))
                            .clipped()
                            .offset(x: 0, y: self.getOffsetForHeaderImage(geometry))
                            .frame(height: 80)
                    }.frame(height: 270)
                    
                    DeviceDetailBannerButton(showARBadge: orchard.isAR, image: orchard.imageName, ARFileName: orchard.ARFileName)
                        .frame(width: 180, height: 180)
                        .offset(x: 0, y: 50)
                    
                }
                
#if os(iOS)
                HStack {
                    FavoriteButton(isSet:  $modelData.orchardData[orchardIndex].isFavorite)
                    
                        .padding(.leading, 35.0)
                    Spacer()
                    
                    Menu {
                        AppleStoreButton(orchard: modelData.orchardData[orchard.id])
                        ShareLink(item: URL(string: "orchard://\(orchard.id)")!)
                    } label: {
                        Image(systemName: "square.and.arrow.up")
                            .font(.title)
                    }
                    .padding(.trailing, 35.0)
                }.offset(x: 0, y: -70)
#else
                HStack {
                    Spacer()

                    ShareLink(item: URL(string: "orchard://\(orchard.id)")!)
                    .padding(.trailing, 35.0)
                }.offset(x: 0, y: -70)
#endif
                
                VStack {
                    HStack {
                        Text(orchard.title)
                            .font(.title)
                            .fontWeight(.medium)
                            .textSelection(.enabled)
                        Spacer()
                    }
                    HStack {
                        Text(orchard.category)
                            .textSelection(.enabled)
                        Spacer()
                        Text(orchard.year)
                            .textSelection(.enabled)
                    }
                    .font(.title3)
                    .foregroundColor(.gray)
                    
                }.offset(x: 0, y: -35)
                    .padding()
                
                ForEach(orchard.sections, id: \.self) { orchard in
                    GroupBox {
                        DisclosureGroup {
                            Rectangle().opacity(0).frame(height: 4)
                            Divider()
                            ForEach(orchard.rows, id: \.self) {
                                orchard in
                                HStack {
                                    Text(orchard.title).fontWeight(.medium).textSelection(.enabled)
                                    Spacer()
                                    Text(orchard.value).textSelection(.enabled)
                                }.padding(2)
                                Divider()
                            }
                            
                        } label: {
                            Label {
                                Text(orchard.header.title).font(.system(size: 18))
                                    .fontWeight(.medium)
                                    .foregroundColor(.primary)
                            } icon: {
                                Image(systemName: orchard.header.symbol).font(.system(size: 18))
                                    .foregroundColor(.accentColor)
                                    
                            }
                        }.padding(.horizontal)
                    }.padding(.horizontal) .groupBoxStyle(TransparentGroupBox())
            }
                Rectangle().opacity(0).frame(height: 10)
            }
#if os(macOS)
            .navigationTitle(orchard.title)
            .toolbar {
                ToolbarItem(placement: .automatic) {
                    FavoriteButton(isSet: $modelData.orchardData[orchardIndex].isFavorite)
                }
                ToolbarItem(placement: .automatic) {
                    AppleStoreButton(orchard: modelData.orchardData[orchard.id])
                }
            }
            
#endif
#if os(iOS)
            .background(Color(.systemGroupedBackground))
#endif
            .edgesIgnoringSafeArea(.top)
        }
    }
}

struct DeviceInfoView_Previews: PreviewProvider {
    static let modelData = ModelData()
    
    static var previews: some View {
        DeviceDetailView(orchard: modelData.orchardData[0])
            .environmentObject(modelData)
    }
}
