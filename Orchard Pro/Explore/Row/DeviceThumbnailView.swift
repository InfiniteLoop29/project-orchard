//
//  DeviceThumbnailView.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-10-08.
//

import SwiftUI

struct DeviceThumbnailView: View {
    @EnvironmentObject var modelData: ModelData
    @Environment(\.colorScheme) var colorScheme
    @Environment(\.openURL) var openURL
    var deviceImage: String
    var backgroundImage: String
    var title: String
    var orchard: Orchard
    func star() -> String {
        if orchard.isFavorite {
            return "star.fill"
        } else {
            return "star"
        }
    }

    func StarColor() -> Color {
        if orchard.isFavorite {
            return .yellow
        } else {
            return .gray
        }
    }

    var orchardIndex: Int {
        modelData.orchardData.firstIndex(where: { $0.id == orchard.id })!
    }

    var body: some View {

        VStack {
            NavigationLink(destination: DeviceDetailView(orchard: orchard)) {
                VStack(alignment: .leading) {
                    Image(orchard.bannerName)
                        .resizable()
                        .blur(radius: 30, opaque: true)
                        .frame(width: 155, height: 155)

                        .overlay(
                            Image(
                            deviceImage)

                            .resizable()
                            .frame(width: 100, height: 100))

                        .overlay(
                            Image(systemName: star())
                                .padding(5.0).font(.system(size: 16)).foregroundColor(StarColor()).background(.thickMaterial, in: Circle()).padding(5), alignment: .topTrailing)
                        .cornerRadius(16)
                    HStack {
                        Text(title)
                            .foregroundColor(.primary)
                            .padding(.leading, 7)

                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                    }
                    .frame(width: 155)
                    .padding(.top, 4)
                }
                .saturation(1.4)
            }.buttonStyle(.plain)
        }
        .contextMenu {
            ContextMenuFavourite(isSet: $modelData.orchardData[orchardIndex].isFavorite)
            Section {
                #if os(macOS)
                Button {
                    openURL(URL(string: "\(orchard.URLScheme)")!)
                       } label: {
                          Text("Open Apple Store")
                       }
                #else
                AppleStoreButton(orchard: modelData.orchardData[orchard.id])
                #endif
                ShareLink(item: URL(string: "orchard://\(orchard.id)")!)
            }
        }
     
    }
}

struct ContextMenuFavourite: View {
    @Binding var isSet: Bool

    var body: some View {
        Button(action: {
            isSet.toggle()
        }) {
            Label("contextfavourite_title", systemImage: "star")
        }
    }
}

struct DeviceThumbnailView_Previews: PreviewProvider {
    static let modelData = ModelData()
    static var orchardData = ModelData().orchardData

    static var previews: some View {
        Group {
            DeviceThumbnailView(deviceImage: "iphone_12", backgroundImage: "iphone_12_wallpaper", title: "iPhone 12", orchard: modelData.orchardData[0])
        }
        .environmentObject(modelData)
    }
}
