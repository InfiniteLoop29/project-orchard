//
//  CategoryView.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-11-12.
//

import SwiftUI
import SharedWithYouCore

struct CategoryDetailView: View {
    @EnvironmentObject var modelData: ModelData
    @Environment(\.openURL) var openURL
    var categoryName: String
    var catergory: Catergory

    var body: some View {
        VStack {
            List {
                ForEach(modelData.orchardData) { orchard in
                    if orchard.category == categoryName {
                        #if os(macOS)
                        GroupBox {
                            NavigationLink(destination: DeviceDetailView(orchard: orchard)
                                .ignoresSafeArea()) {
                                    BrowseLargeRow(orchard: orchard)
                                }
                        }
                        #else
                        NavigationLink(destination: DeviceDetailView(orchard: orchard)
                            .ignoresSafeArea()) {
                                BrowseLargeRow(orchard: orchard)
                            }
                        #endif
                    }
                }
            }
            #if os(iOS)
                .listStyle(.insetGrouped)
            #endif
        } .toolbar {
            ToolbarItem(placement: .automatic) {
                Button {
                    openURL(URL(string: catergory.URLScheme)!)
                } label: {
                    Image(systemName: "safari")
                }
            }
        }
        .navigationTitle(catergory.title)
        #if os(iOS)
            .background(Color(.systemGroupedBackground))
        #endif
    }
}


struct CategoryRowView: View {
    var catergory: Catergory
    @EnvironmentObject var modelData: ModelData
    var body: some View {
        HStack {
            Image(systemName: catergory.imageName)
                .frame(width: 30, height: 30)
            Text(catergory.title)
            Spacer()
        }
        .font(.title2)
    }
}

struct CategoryDetailView_Previews: PreviewProvider {
    static let modelData = ModelData()
    static var previews: some View {
        CategoryDetailView(categoryName: modelData.catergoryData[0].title, catergory: modelData.catergoryData[0])
            .environmentObject(ModelData())
    }
}
