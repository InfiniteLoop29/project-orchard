//
//  ExploreRowView.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-10-09.
//

import SwiftUI

struct ExploreRowView: View {
    let rows = [
        GridItem(.fixed(50)),
    ]

    @EnvironmentObject var modelData: ModelData
    @Environment(\.colorScheme) var colorScheme
    var catergory: Catergory
    var body: some View {
        ZStack {
            if colorScheme == .dark {
                LinearGradient(gradient: Gradient(colors: [.black, Color("GradientColor")]), startPoint: .bottom, endPoint: .top)
            } else {
                LinearGradient(gradient: Gradient(colors: [.white, Color("GradientColor")]), startPoint: .top, endPoint: .bottom)
            }

            VStack(alignment: .leading) {
                HStack {
                    Label {
                        Text(catergory.title)
                            .fontWeight(.medium)
                    } icon: {
                        Image(systemName: catergory.imageName)
                    }
                    .font(.title2)
                    .padding(.leading)

                    Spacer()

                    #if os(iOS)
                        NavigationLink("scrollexplore_seemore_title", destination: CategoryDetailView(categoryName: catergory.title, catergory: catergory))
                            .padding(.trailing)
                    #endif
                }
                .padding(EdgeInsets(top: 20, leading: 0, bottom: 16, trailing: 0))
                ScrollView(.horizontal, showsIndicators: false) {
                    LazyHGrid(rows: rows, alignment: .top, spacing: 15) {
                        ForEach(modelData.orchardData) { orchard in
                            if orchard.category == catergory.title {
                                DeviceThumbnailView(deviceImage: orchard.imageName, backgroundImage: orchard.bannerName, title: orchard.title, orchard: orchard)
                            }
                        }
                    }
                    .padding(.leading, 8)

                    Spacer()
                }

                .padding(.leading, 10.0)
                .frame(height: 215)
            }
        }
        .ignoresSafeArea(edges: .horizontal)
    }
}

struct ExploreRowView_Previews: PreviewProvider {
    static let modelData = ModelData()
    static var previews: some View {
        ExploreRowView(catergory: modelData.catergoryData[1])
            .environmentObject(ModelData())
    }
}
