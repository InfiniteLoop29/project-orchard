//
//  ExploreView.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-10-09.
//

import SwiftUI

struct ExploreView: View {
    @EnvironmentObject var modelData: ModelData
    var body: some View {
        ScrollView {
            VStack {
                #if os(iOS)
                    ExploreBannerView()
                        .offset(y: -17)
                #endif
                ForEach(modelData.catergoryData) { catergory in
                    ExploreRowView(catergory: modelData.catergoryData[catergory.id])
                }
                .navigationTitle("explore_navigation_title")
            }
        }
        #if os(macOS)
        .background(Color("GradientColor"))
        #else
        .background(Color(.systemGroupedBackground))
        #endif
    }
}

struct ExploreView_Previews: PreviewProvider {
    static let modelData = ModelData()
    static var previews: some View {
        NavigationView {
            ExploreView()
                .preferredColorScheme(.light)
        }
        .environmentObject(ModelData())
        .previewInterfaceOrientation(.portrait)
    }
}
