//
//  ExploreBannerView.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-10-17.
//
#if os(iOS)
    import SwiftUI
    import UIKit

    struct ExploreBannerView: View {
        @Environment(\.colorScheme) var colorScheme

        func setupAppearance() {
            UIPageControl.appearance().currentPageIndicatorTintColor = colorScheme == .dark ? .gray : .systemGray
            UIPageControl.appearance().pageIndicatorTintColor = UIColor.gray.withAlphaComponent(0.4)
        }

        @EnvironmentObject var modelData: ModelData
        var body: some View {
            ZStack {
                LinearGradient(gradient: Gradient(colors: [Color(colorScheme == .dark ? .black : .systemGroupedBackground), colorScheme == .dark ? Color(.systemGray6) : .white]), startPoint: .bottom, endPoint: .top)
                    .frame(height: 260)
                VStack {
                    TabView {
                        ZStack {
                            Image("apple_october_event")
                                .frame(minWidth: 100, idealWidth: 150, maxWidth: 500, minHeight: 50, idealHeight: 180, maxHeight: 180)
                                .cornerRadius(16)

                            HStack {
                                Spacer()

                                VStack {
                                    Text("What's New")
                                        .font(.system(size: 30))
                                        .fontWeight(.medium)
                                        .foregroundColor(.white)
                                        .padding(2)

                                        .padding(.trailing, 6)
                                    HStack {
                                        Text("Swipe to find out more")
                                            .foregroundColor(.white)
                                    }
                                    .font(.title3)
                                }
                                .frame(maxWidth: 5000, maxHeight: 180)
                                Spacer()
                            }
                        }
                        .padding()
                        ForEach(modelData.orchardData) { orchard in
                            if orchard.id == 1 || orchard.id == 2 || orchard.id == 3 {
                                TabDeviceThumbnailView(deviceImage: orchard.imageName, backgroundImage: orchard.bannerName, title: orchard.title, orchard: orchard)
                            }
                        }
                    }
                    .tabViewStyle(.page)

                    .onAppear {
                        setupAppearance()
                    }

                }.frame(height: 260)
                    .background(Color(.systemGroupedBackground))
            }
        }
    }

// MARK: TabView

struct TabDeviceThumbnailView: View {
    @Environment(\.colorScheme) var colorScheme
    var deviceImage: String
    var backgroundImage: String
    var title: String
    var orchard: Orchard
    @EnvironmentObject var modelData: ModelData
    func star() -> String {
        if orchard.isFavorite {
            return "star.fill"
        } else {
            return "star"
        }
    }

    func StarColor() -> Color {
        if orchard.isFavorite {
            return .yellow
        } else {
            return .gray
        }
    }

    var orchardIndex: Int {
        modelData.orchardData.firstIndex(where: { $0.id == orchard.id })!
    }

    var body: some View {
        HStack {
            NavigationLink(destination: DeviceDetailView(orchard: orchard)) {
                ZStack {
                    DetailBannerView(image: orchard.bannerName)
                        .frame(minWidth: 100, idealWidth: 150, maxWidth: 500, minHeight: 50, idealHeight: 180, maxHeight: 180)
                        .cornerRadius(16)
                        .saturation(1.5)

                    HStack {
                        Spacer()

                        ZStack {
                            Image(orchard.imageName)
                                .resizable()
                                .frame(width: 100, height: 100)
                                .padding()
                                .frame(width: 120, height: 120)
                                .background(.regularMaterial, in:
                                    RoundedRectangle(cornerRadius: 20))

                                .shadow(radius: 5)
                        }
                        .padding(.leading)

                        VStack {
                            Text(title)
                                .font(.system(size: 30))
                                .fontWeight(.medium)
                                .foregroundColor(.white)
                                .padding(2)

                                .padding(.trailing, 6)
                            HStack {
                                Text(orchard.category)
                                    .foregroundColor(.white)
                                Text("•")
                                    .foregroundColor(.white)
                                Text(orchard.year)
                                    .foregroundColor(.white)
                            }
                            .font(.title3)
                        }
                        .frame(maxWidth: 5000, maxHeight: 180)
                        Spacer()
                    }
                }
                .padding()
            }
        }        .contextMenu {
            ContextMenuFavourite(isSet: $modelData.orchardData[orchardIndex].isFavorite)
            Section {
                AppleStoreButton(orchard: modelData.orchardData[orchard.id])
                ShareLink(item: URL(string: "orchard://\(orchard.id)")!)
            }
        }

    }
}


    struct ExploreBannerView_Previews: PreviewProvider {
        static var previews: some View {
            ExploreBannerView()
                .environmentObject(ModelData())
                .preferredColorScheme(.light)
        }
    }
#endif
