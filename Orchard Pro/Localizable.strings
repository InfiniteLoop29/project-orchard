/* 
  Localizable.strings
  Orchard

  Created by Drake Jordan on 2022-01-21.
  
*/

// Version

"preferences_version" = "Orchard Version 0.1.8 (82) DEV";

// Navigation Strings

"orchard_navigation_title" = "Orchard";

"explore_navigation_title" = "Explore";

"devices_navigation_title" = "Browse";

"preferences_navigation_title" = "Preferences";

"catergories_sidebar_title" = "Categories";

// Devices Strings

"devices_tab_hidden_text" = "Current tab";

"devices_list_tab_title" = "List";

"devices_more_tab_title" = "More";

"devices_filter_favorites_title" = "Show favourites only";

"devices_filter_sort_text" = "Sort by";

"devices_filter_sort_year_title" = "Year descending";

"devices_filter_title" = "Filter";

"devices_search_query_title" = "Search devices...";

// Preferences Strings

"preferences_focus_title" = "Focus mode";

"preferences_focus_alert_text" = "This will display a more focused version of the app, great if you need to work or concentrate";

"preferences_focus_alert_ok_text" = "Ok";

"preferences_info_title" = "Info";

"preferences_faq_title" = "FAQ";

"preferences_maker_title" = "Meet the maker";

"preferences_tips_title" = "Tips";

"preferences_onboarding_title" = "Show onboarding";

"preferences_version_title" = "Show app version";

"preferences_version_alert_ok_text" = "Ok";

"preferences_contact_title" = "Contact";

"preferences_contactemail_title" = "Contact me";

"preferences_contactemail_alert_text" = "Please add an account in the Mail app to continue";

"preferences_contactemail_alert_ok_text" = "Ok";

"preferences_bug_title" = "Report a bug";

"preferences_bug_alert_text" = "Please add an account in the Mail app to continue";

"preferences_bug_alert_ok_text" = "Ok";

// Onboarding Strings

"onboarding_welcome_title" = "Welcome to Orchard!";

"onboarding_block_one_title" = "Detailed Device Info";

"onboarding_block_one_text" = "Find all of the info on your favourite Apple devices in one place.";

"onboarding_block_two_title" = "AR Compatibility";

"onboarding_block_two_text" = "See compatible apple device in all of their AR glory.";

"onboarding_block_three_title" = "Useful Tools";

"onboarding_block_three_text" = "Orchard is full of useful tools, so you can get the most detailed insights.";

"onboarding_block_four_title" = "iCloud Sync";

"onboarding_block_four_text" = "Automatically syncs your data with iCloud so it's up to date on all of your devices.";

"onboarding_swipe_title" = "Swipe down to continue";

// Tips Strings

"tips_footer_text" = "Tips are greatly appreciated, App Store reviews also help support development!";

// FAQ Strings

"faq_block_one_title" = "What devices are included in Orchard?";

"faq_block_one_text" = "You will find devices going back to 2015, with more being added all of the time!";

"faq_block_two_title" = "Why are the tools locked?";

"faq_block_two_text" = "As an individual app developer, I offer most of my app for free, but I include the optional tools to support me and give you a bonus!";

"faq_block_three_title" = "How do I preview a device in AR?";

"faq_block_three_text" = "If the device supports AR Quick Look in Orchard, you will see an AR badge on the device photo. Just click on it to start the experience!";

"faq_block_four_title" = "What devices support the Orchard app?";

"faq_block_four_text" = "You can find Orchard on the iPhone, iPad, Mac, Apple Watch, and Apple TV. All in the App Store";

// Meet Me Strings

"meet_title" = "👋🏻 Hey! I'm Drake";

"meet_text" = "Hi! I'm an indie app developer from Canada and this is my first ever app! I have been interested in app development for a long time and I'm so happy that I get to show you my project!";

// Context Favourite Menu Strings

"contextfavourite_title" = "Favourite";

// Share Button Strings

"sharebutton_title" = "Share Orchard link";

// Apple Store Button Strings

"applestorebutton_title" = "Open Apple Store";

// Scroll Explore Strings

"scrollexplore_seemore_title" = "See more";

// Whats New Strings

"whatsnew_title" = "What's New?";

"whatsnew_text" = "Swipe to find out more";
