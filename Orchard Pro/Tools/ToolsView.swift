//
//  ToolsView.swift
//  Orchard
//
//  Created by Drake Jordan on 2022-01-29.
//

import SwiftUI

struct ToolsView: View {
    let fade =  Gradient(colors: [Color.clear, Color.black])
    @StateObject private var modelData = ModelData()
    var body: some View {
        VStack {
            ScrollView {
                NavigationLink(destination: HistoricalDataView().navigationTitle("Historical Data")) {
                    GroupBox {
                        VStack {
                            HStack {
                                Label {
                                    Text("Historical Data")
                                        .fontWeight(.semibold)
                                } icon: {
                                    Image(systemName: "clock")
                                }
                                .foregroundColor(.accentColor)

                                Spacer()
                                Image(systemName: "chevron.right")
                                    .foregroundColor(.secondary)
                            }
                            Text("IMAGE GOES HERE")
                                .frame(width: 80, height: 80)
                        }
                    }
                    .padding()
                    .groupBoxStyle(TransparentGroupBox())
                }.buttonStyle(.plain)

                NavigationLink(destination: CompareWrapper().navigationTitle("Compare")) {
                    GroupBox {
                        VStack {
                            HStack {
                                Label {
                                    Text("Compare")
                                        .fontWeight(.semibold)
                                } icon: {
                                    Image(systemName: "laptopcomputer.and.iphone")
                                }
                                .foregroundColor(.accentColor)

                                Spacer()
                                Image(systemName: "chevron.right")
                                    .foregroundColor(.secondary)
                            }
                            Text("IMAGE GOES HERE")
                                .frame(width: 80, height: 80)
                        }
                    }
                    .padding()
                    .groupBoxStyle(TransparentGroupBox())
                }.buttonStyle(.plain)
                NavigationLink(destination: DictionaryView().navigationTitle("Dictionary")) {
                    GroupBox {
                        VStack {
                            HStack {
                                Label {
                                    Text("Dictionary")
                                        .fontWeight(.semibold)
                                } icon: {
                                    Image(systemName: "book")
                                }
                                .foregroundColor(.accentColor)

                                Spacer()
                                
                                
                                Image(systemName: "chevron.right")
                                    .foregroundColor(.secondary)
                            }
                                            }
                    }
                    .padding()
                    .groupBoxStyle(TransparentGroupBox())
                }.buttonStyle(.plain)
                #if os(iOS)
                NavigationLink(destination: ARGalleryView().navigationTitle("AR Gallery")) {
                    GroupBox {
                        VStack {
                            HStack {
                                Label {
                                    Text("AR Gallery")
                                        .fontWeight(.semibold)
                                } icon: {
                                    Image(systemName: "arkit")
                                }
                                .foregroundColor(.accentColor)

                                Spacer()
                                Image(systemName: "chevron.right")
                                    .foregroundColor(.secondary)
                            }
                            Text("IMAGE GOES HERE")
                                .frame(width: 80, height: 80)
                        }
                    }
                    .padding()
                    .groupBoxStyle(TransparentGroupBox())
                }
                #endif
            }
            #if os(iOS)
                .navigationBarTitle("Tools", displayMode: .large)
            #endif
        }
        #if os(iOS)
            .background(Color(.systemGroupedBackground))
        #endif
    }
}

struct ToolsView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            ToolsView()
                .navigationTitle("Tools")
        }
    }
}
