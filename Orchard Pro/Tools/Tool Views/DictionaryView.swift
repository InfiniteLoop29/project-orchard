//
//  DictionaryView.swift
//  Orchard
//
//  Created by Drake Jordan on 2022-02-28.
//

import SwiftUI

struct DictionaryView: View {
    @StateObject private var modelData = ModelData()
    @State private var queryString = ""

    var filteredEntries: [Dict] {
        modelData.dictionaryData.filter { dict in
            if !queryString.isEmpty {
                return dict.key.localizedCaseInsensitiveContains(queryString)
            } else {
                return true
            }
        }
    }

    var body: some View {
        VStack {
            List {
                ForEach(filteredEntries) { Dict in
                    
                    DisclosureGroup(Dict.key) {
                        GroupBox {
                            Text(Dict.definition)
                        }
                    }
                    #if os(macOS)
                    Divider()
                    #endif
                }
            }
            #if os(iOS)
            .listStyle(.insetGrouped)
            #endif
            .textSelection(.enabled)
        }
        .searchable(text: $queryString, prompt: "Search definitions...")
    }
}

struct DictionaryView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            DictionaryView()
                .navigationTitle("Dictionary")
        }
    }
}
