//
//  HistoricalDataView.swift
//  Orchard
//
//  Created by Drake Jordan on 2022-01-29.
//

import Charts
import SwiftUI

struct HistoricalDataView: View {
    var body: some View {
        GroupBox {
            Image(systemName: "exclamationmark.triangle.fill")
                .font(Font.system(size: 80))
            Text("This view is currently under construction")
                .fontWeight(.semibold)
                .padding(.top)
                .multilineTextAlignment(.center)
        }
        .frame(width: 200, height: 200)
        .foregroundColor(.secondary)
        .padding()
    }
}

struct HistoricalDataView_Previews: PreviewProvider {
    static var previews: some View {
        HistoricalDataView()
    }
}
