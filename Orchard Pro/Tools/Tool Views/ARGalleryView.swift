//
//  ARGalleryView.swift
//  Orchard
//
//  Created by Drake Jordan on 2022-03-20.
//

import SwiftUI

struct ARGalleryView: View {
    @EnvironmentObject var modelData: ModelData
    let columns = [GridItem(.flexible()), GridItem(.flexible())]

    var body: some View {
        ScrollView {
            LazyVGrid(columns: columns) {
                ForEach(modelData.orchardData) { orchard in
                    if orchard.isAR == true {
                        /*
                        VStack {
                            DeviceDetailBannerButton(showARBadge: orchard.isAR, image: orchard.deviceImage, ARFileName: orchard.ARFileName)
                                .padding(.horizontal)
                            Text(orchard.title)
                                .fontWeight(.medium)
                                .background(.ultraThinMaterial)
                                .padding()
                                .offset(y: -10)
                        } */
                    }
                }
            }.padding(.horizontal)
        }
        #if os(iOS)
            .background(Color(.systemGroupedBackground))
        #endif
    }
}

struct ARGalleryView_Previews: PreviewProvider {
    static var previews: some View {
        ARGalleryView()
            .environmentObject(ModelData())
    }
}
