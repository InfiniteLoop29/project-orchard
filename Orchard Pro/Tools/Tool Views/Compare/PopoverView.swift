//
//  PopoverView.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-08-31.
//

import SwiftUI

#if os(macOS)
struct PopoverView: View {
    @EnvironmentObject var modelData: ModelData
    @State private var selection = "TV"
    @Binding var isPopover: Bool
    let pickerValues = ["iPhone", "iPad", "Watch", "MacBook", "iMac", "Desktop", "AirPods", "HomePod", "TV", "Accessories", "Displays", "iPod", "Server"]
    func pickerContent() -> some View {
        ForEach(pickerValues, id: \.self) {
            Text($0)
        }
    }
    
    var placeHolderINT: Int = 1

    var body: some View {
        VStack {
            Picker("Category:", selection: $selection) {
                pickerContent()
            }
            ScrollView {
                ForEach(modelData.orchardData) { orchard in
                    if orchard.category == selection.description {
                        VStack {
                            Button {
                                print("")
                            } label: {
                                if placeHolderINT == orchard.id {
                                    PopoverRow(imageName: orchard.imageName, title: orchard.title, isSelected: true)
                                } else {
                                    PopoverRow(imageName: orchard.imageName, title: orchard.title, isSelected: false)
                                }
                            }.buttonStyle(.plain)
                        }
                    }
                }
            }
            Button("Confirm") {
                self.isPopover.toggle()
            }.padding(2)
        }.padding()
    }
}

struct PopoverRow: View {
    var imageName: String
    var title: String
    var isSelected: Bool
    var body: some View {
        if isSelected {
            GroupBox {
                HStack {
                    Label {
                        Text(title)
                            .foregroundColor(.white)
                    } icon: {
                        Image(imageName)
                            .resizable()
                            .frame(width: 20, height: 20)
                    }
                    Spacer()
                }.frame(width: 210)
            }.groupBoxStyle(SelectedGroupBox())
        } else {
            GroupBox {
                HStack {
                    Label {
                        Text(title)
                    } icon: {
                        Image(imageName)
                            .resizable()
                            .frame(width: 20, height: 20)
                    }
                    Spacer()
                }.frame(width: 210)
            }
        }
    }
}
#endif
