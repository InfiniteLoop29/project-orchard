//
//  Rows.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-08-31.
//

import SwiftUI

struct DeviceDetailHeader: View {
    var header: String
    var body: some View {
        VStack {
            HStack {
                Text(header).font(.title3).foregroundColor(.secondary).fontWeight(.medium).textCase(.uppercase)
                Spacer()
            }
        }
    }
}

struct DeviceDetailRow: View {
    #if os(iOS)
        @Environment(\.horizontalSizeClass) private var horizontalSizeClass
    #endif
    var title: String
    var detail: String
    var body: some View {
        #if os(iOS)
            if horizontalSizeClass == .regular {
                HStack(spacing: 10) {
                    Text(title)
                        .font(.headline)
                        .frame(maxWidth: .infinity, alignment: .trailing)
                    Text(detail)
                        .font(.body)
                        .frame(maxWidth: .infinity, alignment: .leading)
                }.padding(.vertical, 4)
            } else {
                VStack(spacing: 10) {
                    Text(title)
                        .multilineTextAlignment(.center)
                        .font(.headline)
                    Text(detail)
                        .multilineTextAlignment(.center)
                        .font(.body)
                }.padding(.vertical, 4)
            }
        #else
            HStack(spacing: 10) {
                Text(title)
                    .font(.headline)
                    .frame(maxWidth: .infinity, alignment: .trailing)
                Text(detail)
                    .font(.body)
                    .frame(maxWidth: .infinity, alignment: .leading)
            }.padding(.vertical, 4)
        #endif
    }
}
