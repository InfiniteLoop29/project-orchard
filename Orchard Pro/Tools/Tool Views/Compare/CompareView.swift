//
//  CompareView.swift
//  Orchard
//
//  Created by Drake Jordan on 2022-01-29.
//

import SwiftUI

struct CompareWrapper: View {
    @EnvironmentObject var modelData: ModelData
    #if os(iOS)
        @Environment(\.horizontalSizeClass) private var horizontalSizeClass
    #endif
    var body: some View {
        #if os(iOS)
            if horizontalSizeClass == .regular {
                CompareView(orchardOne: $modelData.orchardData[0], orchardTwo: $modelData.orchardData[1])
                    .toolbar {
                        ToolbarItem(placement: .automatic) {
                            HStack {
                                Button(action: {modelData.compareSettings.tabCount += 1}) { Image(systemName: "plus") }
                                    .disabled(modelData.compareSettings.tabCount > 2)
                                #if os(macOS)
                                    Divider().padding(.vertical, 2)
                                #endif
                                Button(action: {modelData.compareSettings.tabCount -= 1}) { Image(systemName: "minus") }
                                    .disabled(modelData.compareSettings.tabCount < 3)
                                
                            }
                        }
                    }
            } else {
                CompareView(orchardOne: $modelData.orchardData[0], orchardTwo: $modelData.orchardData[1])
            }
        #else
            VStack {
                CompareView(orchardOne: $modelData.orchardData[0], orchardTwo: $modelData.orchardData[1])
                    .toolbar {
                        ToolbarItem(placement: .automatic) {
                            HStack {
                                Button(action: {modelData.compareSettings.tabCount += 1}) { Image(systemName: "plus") }
                                    .disabled(modelData.compareSettings.tabCount > 2)
                                #if os(macOS)
                                    Divider().padding(.vertical, 2)
                                #endif
                                Button(action: {modelData.compareSettings.tabCount -= 1}) { Image(systemName: "minus") }
                                    .disabled(modelData.compareSettings.tabCount < 3)
                                
                            }
                        }
                    }
            }
        #endif
    }
}

struct CompareView: View {
    @EnvironmentObject var modelData: ModelData
    @State var isPopover = false
    @Binding var orchardOne: Orchard
    @Binding var orchardTwo: Orchard
    var body: some View {
        ScrollView {
            HStack {
                if modelData.compareSettings.tabCount == 2 {
                    CompareRow(orchard: $orchardTwo)
                    #if os(macOS)
                        Spacer()
                        Divider()
                            .padding(.vertical)
                        Spacer()
                    #endif
                    CompareRow(orchard: $orchardOne)
                } else {
                    CompareRow(orchard: $orchardTwo)
                    #if os(macOS)
                        Spacer()
                        Divider()
                            .padding(.vertical)
                        Spacer()
                    #endif
                    CompareRow(orchard: $orchardOne)
                    #if os(macOS)
                        Spacer()
                        Divider()
                            .padding(.vertical)
                        Spacer()
                    #endif
                    CompareRow(orchard: $orchardOne)
                }
            }
        }
        #if os(iOS)
        .background(Color(.systemGroupedBackground))
        #endif
    }
}

struct CompareRow: View {
    @State var isPopover = false
    @Binding var orchard: Orchard
    var body: some View {
        VStack {
            Rectangle().opacity(0).frame(height: 15)
            DeviceHeaderView(isPopover: isPopover, title: orchard.title, image: orchard.imageName)
                .groupBoxStyle(TransparentGroupBox())
            #if os(iOS)
                .padding(.leading, 8)
            #endif
            ForEach(orchard.sections, id: \.self) { orchard in
                GroupBox {
                    VStack {
                        DeviceDetailHeader(header: orchard.header.title)
                        ForEach(orchard.rows, id: \.self) { orchard in
                            DeviceDetailRow(title: orchard.title, detail: orchard.value)
                        }
                    }
                }
            }
            #if os(macOS)
            .groupBoxStyle(NSDarkGroupBox()).padding(.horizontal)
            #else
            .groupBoxStyle(TransparentGroupBox()).padding(.leading, 8)
            #endif
            Spacer()
        }
    }
}

struct DeviceHeaderView: View {
    @EnvironmentObject var modelData: ModelData
    @State var isPopover = false
    var title: String
    var image: String
    var body: some View {
        GroupBox {
            VStack {
                HStack {
                    Spacer()
                    Image(image).resizable().frame(width: 80, height: 80)
                    Spacer()
                }
                Text(title).multilineTextAlignment(.center).fontWeight(.medium)
                Button(action: { self.isPopover.toggle() }) {
                    #if os(iOS)
                        RoundedRectangle(cornerRadius: 8).frame(height: 30).foregroundColor(Color("GradientColor")).overlay {
                            Text("Choose Device")
                                .foregroundColor(.accentColor).font(.callout)
                        }
                    #else
                        Text("Choose Device")
                    #endif

                }.popover(isPresented: self.$isPopover, arrowEdge: .bottom) {
                    #if os(macOS)
                        PopoverView(isPopover: $isPopover)
                    #endif
                }
                #if os(iOS)
                .buttonStyle(PlainButtonStyle())
                #endif
            }
            #if os(macOS)
            .padding(.vertical)
            #endif
        }
        #if os(macOS)
        .groupBoxStyle(NSDarkGroupBox()).padding(.horizontal)
        #endif
    }
}
