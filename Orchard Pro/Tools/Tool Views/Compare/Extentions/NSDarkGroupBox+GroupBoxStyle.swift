//
//  NSDarkGroupBox+GroupBoxStyle.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-08-31.
//

import SwiftUI

#if os(macOS)
    struct NSDarkGroupBox: GroupBoxStyle {
        @Environment(\.colorScheme) var colorScheme
        func makeBody(configuration: Configuration) -> some View {
            configuration.content
                .frame(maxWidth: .infinity)
                .padding()
                .background(RoundedRectangle(cornerRadius: 6).fill(Color(.quaternaryLabelColor)))
                .overlay(
                    RoundedRectangle(cornerRadius: 5)
                        .stroke(.quaternary, lineWidth: 1)
                )
                .overlay(configuration.label.padding(.leading, 4), alignment: .topLeading)
        }
    }
#endif
