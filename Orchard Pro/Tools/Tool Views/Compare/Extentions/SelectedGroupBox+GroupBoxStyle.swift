//
//  SwiftUIView.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-08-31.
//

import SwiftUI

#if os(macOS)
struct SelectedGroupBox: GroupBoxStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.content
            .frame(width: 220)
            .padding(.vertical, 5)
            .background(RoundedRectangle(cornerRadius: 6).fill(Color(.controlAccentColor)))
            .overlay(
                RoundedRectangle(cornerRadius: 5)
                    .stroke(.quaternary, lineWidth: 1)
            )
     
    }
}
#endif
