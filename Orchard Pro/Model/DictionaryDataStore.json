[
  {
    "id": 0,
    "key": ".Mac",
    "definition": "A suite of internet services created by Apple for users of Mac computers. In 2008, .Mac (pronounced “dot Mac”) was replaced by MobileMe. See also MobileMe."
  },
  {
    "id": 1,
    "key": "3D Touch",
    "definition": "A feature on some iPhone models that lets you interact with items on the screen in different ways depending on how hard you press. For example, in the mailbox list in Mail, press a message to preview it, then press a little deeper to open it."
  },
  {
    "id": 2,
    "key": "A13 Bionic",
    "definition": "The 64-bit system on a chip (SoC) in iPhone 11, iPhone 11 Pro, iPhone 11 Pro Max, and iPhone SE (2nd generation)."
  },
  {
    "id": 3,
    "key": "A14 Bionic",
    "definition": "The 64-bit system on a chip (SoC) in iPhone 12 mini, iPhone 12, iPhone 12 Pro, iPhone 12 Pro Max, and iPad Air (4th generation)."
  },
  {
    "id": 4,
    "key": "Activity Monitor",
    "definition": "An Apple app that lets you get details about the processor, apps, disks, memory, and network activity on your Mac."
  },
  {
    "id": 5,
    "key": "administrator",
    "definition": "On Mac computers, an administrator is a user who can create, delete, and modify other users and groups; install software; and change system settings. If a Mac has only a single user, that user is automatically the administrator. See also user."
  },
  {
    "id": 6,
    "key": "AFP",
    "definition": "Apple Filing Protocol (AFP). A client and server protocol used by the Apple file service to share files and network services. AFP uses TCP/IP and other protocols to support communication between computers on a network. You can no longer share files using AFP if your volume is formatted with Apple File System (APFS). See also Apple File System (APFS)."
  },
  {
    "id": 7,
    "key": "Afterburner",
    "definition": "A hardware accelerator card in the Mac Pro (2019 and later) that allows video editors to edit without having to create a proxy. Afterburner is built with an FPGA, or programmable ASIC, and can process up to 6.3 billion pixels per second, while handling up to three streams of 8K ProRes RAW or 12 streams of 4K ProRes RAW video. See the Mac Pro website."
  },
  {
    "id": 8,
    "key": "AirDrop",
    "definition": "An Apple technology that lets you share files from your device with other nearby Apple devices over Wi-Fi."
  },
  {
    "id": 9,
    "key": "AirPlay",
    "definition": "An Apple technology that lets you securely stream and share videos, photos, music, and more from Apple devices to your Apple TV, Mac, favorite speakers (such as HomePod), and certain smart TVs. See the AirPlay website."
  },
  {
    "id": 10,
    "key": "AirPlay Mirroring",
    "definition": "An Apple technology that lets you see whatever is on your iOS device, iPadOS device, or Mac computer on a bigger screen. See the AirPlay website."
  },
  {
    "id": 11,
    "key": "AirPods",
    "definition": "True wireless in-ear headphones from Apple. AirPods feature a universal fit, and provide high-quality AAC audio and microphones for making calls or talking to Siri. See the AirPods website."
  },
  {
    "id": 12,
    "key": "AirPods Max",
    "definition": "Wireless over-ear headphones from Apple. AirPods Max provide high-fidelity audio, Active Noise Cancellation and Transparency mode, and microphones for making calls or talking to Siri. See the AirPods website."
  },
  {
    "id": 13,
    "key": "AirPods Pro",
    "definition": "True wireless in-ear headphones from Apple. AirPods Pro feature a customizable fit with silicone tips, and provide high-quality AAC audio, Active Noise Cancellation and Transparency mode, and microphones for making calls or talking to Siri. See the AirPods website."
  },
  {
    "id": 14,
    "key": "AirPort",
    "definition": "Apple’s hardware and software implementation of Wi-Fi networking. The AirPort product line included the AirPort Express, AirPort Extreme, and AirPort Time Capsule base stations, and the AirPort Utility app. AirPort products were discontinued in 2018."
  },
  {
    "id": 15,
    "key": "AirPort Disk",
    "definition": "An external disk connected to an AirPort Extreme or AirPort Time Capsule base station, so that computers on the network can access the contents of the disk. See also AirPort."
  },
  {
    "id": 16,
    "key": "AirPort Express",
    "definition": "A wireless router from Apple that provides internet connectivity, and is simpler and more compact than the AirPort Extreme base station. See also AirPort Extreme."
  },
  {
    "id": 17,
    "key": "AirPort Extreme",
    "definition": "A wireless router from Apple that provides high-speed internet connectivity. See also AirPort Express and AirPort Time Capsule."
  },
  {
    "id": 18,
    "key": "AirPort Time Capsule",
    "definition": "A wireless router from Apple that provides high-speed wireless internet connectivity and automatically backs up all your Mac computers wirelessly using Time Machine. See the AirPort Support website. See also Time Machine."
  },
  {
    "id": 19,
    "key": "AirPrint",
    "definition": "An Apple technology that lets you print photos and documents from your iOS device, iPadOS device, or Mac computer without the need to download or install drivers."
  },
  {
    "id": 20,
    "key": "AirTag",
    "definition": "A device from Apple that lets you locate items using your iPhone or the Find My network. See the AirTag website."
  },
  {
    "id": 21,
    "key": "AirTunes",
    "definition": "An Apple technology that let you wirelessly play music from your Apple device to external speakers. AirTunes was replaced by AirPlay in 2010. See also AirPlay."
  },
  {
    "id": 22,
    "key": "album",
    "definition": "In the Photos app from Apple, a collection of photos and videos from your library. See also Photos."
  },
  {
    "id": 23,
    "key": "Aperture",
    "definition": "A professional-level app from Apple used by professional photographers to organize, edit, and share digital photos on the Mac. Aperture is no longer developed by Apple. See Photos."
  },
  {
    "id": 24,
    "key": "APFS",
    "definition": "The on-disk volume format used for Apple File System. See also Apple File System."
  },
  {
    "id": 25,
    "key": "App clip",
    "definition": "A small part of an iOS or iPadOS app that’s discoverable the moment you need it and is focused on a specific task."
  },
  {
    "id": 26,
    "key": "App Exposé",
    "definition": "An iPadOS feature that lets you see all the open windows for an app."
  },
  {
    "id": 27,
    "key": "App Library",
    "definition": "The space at the end of your Home Screen pages that automatically organizes all of your iPhone, iPad, and iPod touch apps into one simple, easy‑to‑navigate view."
  },
  {
    "id": 28,
    "key": "App Nap",
    "definition": "An Apple technology that helps your Mac conserve system resources and battery life when multiple apps are open at the same time. App Nap slows down open apps that you’re not using."
  },
  {
    "id": 29,
    "key": "App Store",
    "definition": "An online store from Apple that lets you browse, purchase, and install apps made by Apple and third-party developers for iPhone, iPad, iPod touch, and Apple TV. See also Mac App Store."
  },
  {
    "id": 30,
    "key": "App Switcher",
    "definition": "A feature on iPhone, iPad, and iPod touch that shows all open apps so you can quickly switch from one app to another."
  },
  {
    "id": 31,
    "key": "Apple Arcade",
    "definition": "A subscription service from Apple that lets you download and play games on your Mac, iPhone, iPad, and Apple TV. Apple Arcade availability varies by country or region."
  },
  {
    "id": 32,
    "key": "Apple Books",
    "definition": "An Apple app (formerly called “iBooks”) that lets you search for, download, and enjoy books and audiobooks on your iPhone, iPad, iPod touch, and Mac. (Features and content may vary by country or region.)"
  },
  {
    "id": 33,
    "key": "Apple Business Manager",
    "definition": "A simple, web-based portal from Apple that allows IT administrators to deploy iPhone, iPad, and Mac devices in their organization. Administrators can set up devices, give employees and contractors access to Apple services, and provide apps and books, all from one place. See the Apple at Work website."
  },
  {
    "id": 34,
    "key": "Apple Card",
    "definition": "A kind of credit card, created by Apple, available in the Wallet app on iPhone. Apple Card is also available as a physical card for use at locations where Apple Pay isn’t accepted yet. See the Apple Card website."
  },
  {
    "id": 35,
    "key": "Apple Cash",
    "definition": "A prepaid card in Wallet that stores the money you get paid with Apple Pay. You can use the Apple Cash card to send money to friends, and to make purchases with Apple Pay in stores, in apps, and on websites. See the Apple Pay website."
  },
  {
    "id": 36,
    "key": "Apple Configurator 2",
    "definition": "An Apple app for Mac computers used to configure and deploy multiple iPhone, iPad, and Apple TV devices for use in a school, business, or organization. See the Apple at Work website."
  },
  {
    "id": 37,
    "key": "Apple File System",
    "definition": "A file system from Apple for iOS, iPadOS, macOS, tvOS, and watchOS. It’s used to store data on Apple devices and is optimized for flash and solid-state drive storage."
  },
  {
    "id": 38,
    "key": "Apple Fitness+",
    "definition": "A subscription service from Apple that gives you access to a catalog of workouts, including Cycling, Strength, Treadmill (running and walking), Yoga, and more. Apple Fitness+ availability varies by country or region. See the Apple Fitness+ website."
  },
  {
    "id": 39,
    "key": "Apple ID",
    "definition": "The account you use to access Apple services such as the App Store, Apple Music, iCloud, iMessage, FaceTime, and more. It includes the email address and password you use to sign in as well as all the contact, payment, and security details you use across Apple services. See the Apple ID website."
  },
  {
    "id": 40,
    "key": "Apple Inc.",
    "definition": "Apple Inc. is a technology corporation founded in California in 1976. Headquartered in Cupertino, California, Apple products include Mac computers, iPhone, iPad, iPod, Apple Watch, Apple TV, macOS, iOS, iPadOS, watchOS, tvOS, and more."
  },
  {
    "id": 41,
    "key": "Apple M1 Chip",
    "definition": "A System on a Chip (SoC) technology that combines powerful technologies and a unified memory architecture to improve performance and efficiency on certain models of Mac computers and iPad devices."
  },
  {
    "id": 42,
    "key": "Apple Music",
    "definition": "A subscription service from Apple that lets you stream millions of songs, the Apple Music 1 radio station, interviews, live shows, and exclusive video content across all your devices and from the web. (Features and content may vary by country or region.) See the Apple Music website."
  },
  {
    "id": 43,
    "key": "Apple Music radio",
    "definition": "A collection of world-class radio stations, such as Apple Music 1 and other stations based on a variety of genres, included in Apple Music. (Features and content may vary by country or region.) See also Apple Music."
  },
  {
    "id": 44,
    "key": "Apple News+",
    "definition": "A subscription service from Apple that provides access to current and past issues of hundreds of magazines and leading newspapers. Apple News+ availability varies by country or region."
  },
  {
    "id": 45,
    "key": "Apple Pay",
    "definition": "A service from Apple that lets you make easy, secure, and private payments in stores, in apps, and on websites, and to friends and family, using your Apple Pay-enabled devices. See the Apple Pay website."
  },
  {
    "id": 46,
    "key": "Apple Pencil",
    "definition": "A highly responsive and precise digital stylus used to sketch, draw, or write on the surface of some iPad models. See the Apple Pencil website."
  },
  {
    "id": 47,
    "key": "Apple ProRaw",
    "definition": "An image format that combines the information of a standard RAW format along with iPhone image processing, which gives you more flexibility when editing the exposure, color, and white balance in your photo."
  },
  {
    "id": 48,
    "key": "Apple ProRes",
    "definition": "A family of video codecs from Apple that provide a combination of multistream real-time editing performance, impressive image quality, and reduced storage rates. ProRes codecs are available in a range of data rates and quality levels, and all versions support all frame sizes — including SD, HD, 2K, 4K, and 8K. See the Apple ProRes white paper."
  },
  {
    "id": 49,
    "key": "Apple ProRes RAW",
    "definition": "A video codec from Apple that applies ProRes compression technology (rather than conventional image pixels) to a camera sensorʼs raw image data. As a result, ProRes RAW combines the performance of ProRes with the flexibility of raw video while supporting all frame sizes, including SD, HD, 2K, 4K, and 8K. See the Apple ProRes RAW white paper."
  },
  {
    "id": 50,
    "key": "Apple Remote",
    "definition": "A remote control for use with several Apple products, including Apple TV. In 2015, Apple Remote was replaced by Siri Remote and the Apple TV Remote for use with Apple TV HD and Apple TV 4K. See also Apple TV Remote and Siri Remote."
  },
  {
    "id": 51,
    "key": "Apple Remote Desktop",
    "definition": "An Apple app that lets you manage computers, interact with users, distribute software, create software and hardware reports, and administer several computers at once, all from a single Mac. See the Apple Remote Desktop website."
  },
  {
    "id": 52,
    "key": "Apple School Manager",
    "definition": "A simple, web-based portal from Apple that helps IT administrators deploy iPad and Mac devices in schools. Administrators can set up devices, give students and staff access to Apple services, and provide apps and books, all from one place. See the Deploy Apple devices for Education website."
  },
  {
    "id": 53,
    "key": "Apple Store",
    "definition": "A chain of retail stores owned and operated by Apple, which sell computers, consumer electronics, and accessories. Apple has received a number of architectural awards for its store designs."
  },
  {
    "id": 54,
    "key": "Apple T2 Security Chip",
    "definition": "A technology in late-model Mac computers that provides industry-leading security features, including the foundation for APFS encrypted storage, secure boot, and Touch ID. See Apple T2 Security Chip Overview."
  },
  {
    "id": 55,
    "key": "Apple Thunderbolt Display",
    "definition": "A 27-inch flat-panel display from Apple. The display included LED backlighting, one cable with Thunderbolt and MagSafe connectors, and a 2560 x 1440 pixel resolution. The Apple Thunderbolt Display was discontinued in 2016."
  },
  {
    "id": 56,
    "key": "Apple TV",
    "definition": "A device that delivers live and on-demand access to movies, TV shows, sports, games, apps, podcasts, and your music and photos, right on your high-definition or 4K TV. See the Apple TV website. See also AirPlay."
  },
  {
    "id": 57,
    "key": "Apple TV app",
    "definition": "An Apple app for subscribing to channels, and browsing and watching TV shows, movies, and other content. (Features and content may vary by country or region.) The Apple TV app is available on Apple TV, iPhone, iPad, and Mac, as well as on some smart TVs, gaming consoles, and streaming devices. See the Apple TV App website."
  },
  {
    "id": 58,
    "key": "Apple TV Remote",
    "definition": "A device that works with Apple TV HD and Apple TV 4K. Apple TV Remote includes a clickpad with touch surface (2nd generation) or touch surface (1st generation) that lets you interact with and navigate Apple TV. In countries and regions that support Siri, the device is called Siri Remote. See also Siri Remote."
  },
  {
    "id": 59,
    "key": "Apple TV+",
    "definition": "A streaming service from Apple that lets you watch original shows and movies using the Apple TV app on Apple TV, Mac, iPhone, and iPad, as well as on some smart TVs and streaming devices. Apple TV+ availability varies by country or region."
  },
  {
    "id": 60,
    "key": "Apple Watch",
    "definition": "A personal device from Apple that you wear on your wrist. In addition to offering time-related features, Apple Watch includes apps for health and fitness, communication and sharing, and more. See the Apple Watch website."
  },
  {
    "id": 61,
    "key": "AppleCare",
    "definition": "The service and support program for Apple products. You can purchase an AppleCare protection plan to extend the complimentary coverage for your Apple product. See the AppleCare Products website."
  },
  {
    "id": 62,
    "key": "AppleScript",
    "definition": "A scripting language built into macOS. The AppleScript language consists of commands that can be used to automate repetitive or complex tasks within many apps, including Mail, Safari, and Calendar."
  },
  {
    "id": 63,
    "key": "AppleShare",
    "definition": "File sharing and print server software for AppleTalk networking. It was discontinued in 1999."
  },
  {
    "id": 64,
    "key": "AppleTalk",
    "definition": "Networking protocols developed by Apple to connect Mac computers and printers on a local network. In 2009, Apple completed its transition of Mac networking to use the Internet Protocol (IP). See also Bonjour."
  },
  {
    "id": 65,
    "key": "AppleWorks",
    "definition": "A suite of Apple apps that included word-processing, spreadsheet, graphics, and database apps. AppleWorks was replaced by the iWork suite of productivity apps—Pages, Numbers, and Keynote apps. See the iWork website."
  },
  {
    "id": 66,
    "key": "Aqua",
    "definition": "The graphical user interface and visual theme of macOS."
  },
  {
    "id": 67,
    "key": "ARKit",
    "definition": "A set of software development tools from Apple used to create augmented reality apps and games for iPhone and iPad, letting you superimpose digital information and objects into the real world and interact with them. See the ARKit website."
  },
  {
    "id": 68,
    "key": "Auto Unlock",
    "definition": "A macOS feature that lets you unlock your Mac without entering a password when you’re wearing your authenticated Apple Watch. See also Continuity."
  },
  {
    "id": 69,
    "key": "AutoFill",
    "definition": "A feature of Apple’s Safari web browser that automatically fills in forms on a webpage, using information from your card in the Contacts app and from forms you’ve previously filled in."
  },
  {
    "id": 70,
    "key": "Automated Device Enrollment",
    "definition": "An Apple feature that lets organizations automate mobile device management (MDM) enrollment and simplify initial device setup. Administrators can supervise devices during activation without touching them (using auto advance), and lock MDM enrollment for ongoing management (such as deploying and removing apps). See also Apple School Manager or Apple Business Manager."
  },
  {
    "id": 71,
    "key": "Automator",
    "definition": "An Apple app that lets you create workflows for automating repetitive tasks on a Mac. Automator works with many other Apple and third-party apps."
  },
  {
    "id": 72,
    "key": "Beats",
    "definition": "An Apple app on Android devices that lets you customize and manage your Beats products. See also Beats earphones and headphones and Beats speakers. See the Beats website."
  },
  {
    "id": 73,
    "key": "Beats earphones and headphones",
    "definition": "Wired and wireless in-ear earphones and over-ear or on-ear headphones from Apple. Beats earphones and headphones deliver premium playback with fine-tuned acoustics to maximize clarity, breadth, and balance. See Beats products."
  },
  {
    "id": 74,
    "key": "Beats earphones and headphones",
    "definition": "Wired and wireless in-ear earphones and over-ear or on-ear headphones from Apple. Beats earphones and headphones deliver premium playback with fine-tuned acoustics to maximize clarity, breadth, and balance. See Beats products."
  },
  {
    "id": 75,
    "key": "Beats speakers",
    "definition": "Portable speakers from Apple that let you play audio from your paired Apple device or other Bluetooth-enabled devices. See Beats products."
  },
  {
    "id": 76,
    "key": "Big Sur",
    "definition": "The name for macOS 11"
  },
  {
    "id": 77,
    "key": "Blood Oxygen",
    "definition": "The level of oxygen saturation of your blood. Blood oxygen measurements can be performed with the Blood Oxygen app on certain models of Apple Watch in supported regions. See also Apple Watch."
  },
  {
    "id": 78,
    "key": "Bluetooth File Exchange",
    "definition": "An Apple app that lets you make short-range wireless connections between devices."
  },
  {
    "id": 79,
    "key": "Bonjour",
    "definition": "An Apple technology that lets you discover services easily on a local network. Built into macOS and iOS, Bonjour automatically locates and connects to nearby devices, such as printers and computers."
  },
  {
    "id": 80,
    "key": "burn folder",
    "definition": "A type of folder on Mac computers that contains items you want to burn to a CD or DVD."
  },
  {
    "id": 81,
    "key": "Daily Cash",
    "definition": "A percentage of your Apple Card purchases that you accumulate, added to your Apple Cash card every day. See the Apple Card website."
  },
  {
    "id": 82,
    "key": "Dark Mode",
    "definition": "A feature on Apple devices that provides a color scheme that puts the focus on content and is perfect for low-light conditions."
  },
  {
    "id": 83,
    "key": "Dashboard",
    "definition": "A macOS feature that provided small apps called “widgets.” Dashboard was discontinued in 2019."
  },
  {
    "id": 84,
    "key": "Deep Fusion",
    "definition": "An image processing technology on some iPhone models that uses advanced machine learning to do pixel-by-pixel processing of photos, optimizing for texture, details, and noise in every part of the photo."
  },
  {
    "id": 85,
    "key": "desktop",
    "definition": "The background area of a computer screen. A desktop provides icons, windows, toolbars, files, and folders that you can create, edit, move, and delete."
  },
  {
    "id": 86,
    "key": "desktop picture",
    "definition": "The color, pattern, or picture you use to customize the background area of your display. See also Dynamic Desktop."
  },
  {
    "id": 87,
    "key": "dictation commands",
    "definition": "A macOS feature that lets you edit text and interact with your computer by giving spoken commands."
  },
  {
    "id": 88,
    "key": "Dictionary",
    "definition": "An Apple app that comes with macOS. Dictionary lets you look up definitions and synonyms in a variety of sources including the Oxford American Dictionary and the Apple Dictionary."
  },
  {
    "id": 89,
    "key": "Digital Touch",
    "definition": "A feature in the Messages app that lets you send sketches, taps, heartbeats, and more."
  },
  {
    "id": 90,
    "key": "Disk Utility",
    "definition": "An Apple app that lets you create disk images on a Mac, and also mount, remove, reformat, partition, and repair disks."
  },
  {
    "id": 91,
    "key": "Do Not Disturb",
    "definition": "A feature that lets you silence calls, alerts, and notifications on your iOS device, iPadOS device, Mac computer, or Apple Watch."
  },
  {
    "id": 92,
    "key": "Dock",
    "definition": "A feature of macOS, iOS, and iPadOS, the Dock is a row of icons located along one edge of the screen and is a convenient place to access apps and other items that you use frequently."
  },
  {
    "id": 93,
    "key": "dogcow",
    "definition": "Originally a character in the Cairo dingbat (ornament) font, the dogcow was used to show the orientation and color of the paper in page setup dialogs on Mac computers. Over time the dogcow acquired additional spots and a more bovine appearance. The call of the dogcow is “moof.”"
  },
  {
    "id": 94,
    "key": "Downtime",
    "definition": "A feature that lets you schedule specific times when access to all or certain apps is blocked on your Apple devices or those of a family member, such as a child."
  },
  {
    "id": 95,
    "key": "DVD or CD sharing",
    "definition": "A macOS feature that lets you share a DVD or CD disc in the optical drive of your Mac with another computer on the same network."
  },
  {
    "id": 96,
    "key": "Dynamic Desktop",
    "definition": "A desktop picture that automatically changes throughout the day to match the time of day in your current location."
  },
  {
    "id": 97,
    "key": "EarPods",
    "definition": "Earbud-style headphones from Apple. EarPods with Remote and Mic include a built-in remote that lets you adjust volume, control playback of music and video, and answer or end calls."
  },
  {
    "id": 98,
    "key": "ECG (electrocardiogram)",
    "definition": "A test that can be performed while wearing certain models of Apple Watch in supported regions that records the timing and strength of the electrical signals that make the heart beat. See also Apple Watch."
  },
  {
    "id": 99,
    "key": "El Capitan",
    "definition": "The name for OS X 10.11. See also macOS."
  },
  {
    "id": 100,
    "key": "eMac",
    "definition": "An all-in-one desktop computer introduced by Apple in 2002. It was discontinued in 2006, when it was replaced by an iMac model. See also iMac."
  },
  {
    "id": 101,
    "key": "Emergency SOS",
    "definition": "A feature that lets you call local emergency services from your iPhone or Apple Watch. Emergency SOS can also display your medical information and alert your emergency contacts."
  },
  {
    "id": 102,
    "key": "ExFAT format",
    "definition": "One of two file system formats compatible with Mac and Windows. See also Disk Utility; MS DOS (FAT) format."
  }
]
