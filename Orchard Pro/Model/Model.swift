//
//  OrchardTestData.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-09-30.
//

import Combine
import SwiftUI
#if os(iOS)
    import MessageUI
    import UIKit
#endif

class ModelData: ObservableObject {
    @Published var orchardData: [Orchard] = load("OrchardDataStore.json")
    @Published var deeplink: DeepLinkManager = DeepLinkManager()
    @Published var catergoryData: [Catergory] = load("CategoryDataStore.json")
    @Published var dictionaryData: [Dict] = load("DictionaryDataStore.json")
    @Published var compareSettings = CompareSettings()
    @Published var appSettings = AppSettings()
}

struct CompareSettings: Codable {
     var tabCount: Int = 2
}

struct AppSettings: Codable {
     var colorScheme: Int = 2
}


func load<T: Decodable>(_ filename: String) -> T {
    let data: Data

    guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
    else {
        fatalError("Couldn't find \(filename) in main bundle.")
    }

    do {
        data = try Data(contentsOf: file)
    } catch {
        fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
    }

    do {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    } catch {
        fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
    }
}

// MARK: Structs

struct Dict: Hashable, Codable, Identifiable {
    var id: Int
    var key: String
    var definition: String
}

struct TabModel: Identifiable, Hashable {
    let id = UUID()
    let name: String
    let details: String
    let symbol: String
}

#if os(iOS)

    struct ComposeMailData {
        let subject: String
        let recipients: [String]?
        let message: String
        let attachments: [AttachmentData]?
    }

    struct AttachmentData {
        let data: Data
        let mimeType: String
        let fileName: String
    }

    typealias MailViewCallback = ((Result<MFMailComposeResult, Error>) -> Void)?

    struct MailView: UIViewControllerRepresentable {
        @Environment(\.presentationMode) var presentation
        @Binding var data: ComposeMailData
        let callback: MailViewCallback

        class Coordinator: NSObject, MFMailComposeViewControllerDelegate {
            @Binding var presentation: PresentationMode
            @Binding var data: ComposeMailData
            let callback: MailViewCallback

            init(presentation: Binding<PresentationMode>,
                 data: Binding<ComposeMailData>,
                 callback: MailViewCallback) {
                _presentation = presentation
                _data = data
                self.callback = callback
            }

            func mailComposeController(_ controller: MFMailComposeViewController,
                                       didFinishWith result: MFMailComposeResult,
                                       error: Error?) {
                if let error = error {
                    callback?(.failure(error))
                } else {
                    callback?(.success(result))
                }
                $presentation.wrappedValue.dismiss()
            }
        }

        func makeCoordinator() -> Coordinator {
            Coordinator(presentation: presentation, data: $data, callback: callback)
        }

        func makeUIViewController(context: UIViewControllerRepresentableContext<MailView>) -> MFMailComposeViewController {
            let vc = MFMailComposeViewController()
            vc.mailComposeDelegate = context.coordinator
            vc.setSubject(data.subject)
            vc.setToRecipients(data.recipients)
            vc.setMessageBody(data.message, isHTML: false)
            data.attachments?.forEach {
                vc.addAttachmentData($0.data, mimeType: $0.mimeType, fileName: $0.fileName)
            }
            vc.accessibilityElementDidLoseFocus()
            return vc
        }

        func updateUIViewController(_ uiViewController: MFMailComposeViewController,
                                    context: UIViewControllerRepresentableContext<MailView>) {
        }

        static var canSendMail: Bool {
            MFMailComposeViewController.canSendMail()
        }
    }

#endif

class DeepLinkManager {
    
    enum DeeplinkTarget: Equatable {
         case explore
         case browse
         case tools
         case more
         case details(reference: String)
    }
    
    class DeepLinkConstants {
        static let scheme = "orchard"
        static let host = "com.orchardlinks"
        static let detailsPath = "/details"
        static let query = "id"
    }
    
    
    func manage(_ url: URL) -> DeeplinkTarget {
            guard url.scheme == DeepLinkConstants.scheme,
                  url.host == DeepLinkConstants.host,
                  url.path == DeepLinkConstants.detailsPath,
                  let components = URLComponents(url: url, resolvingAgainstBaseURL: true),
                  let queryItems = components.queryItems
            else { return .explore }
            
            let query = queryItems.reduce(into: [String: String]()) { (result, item) in
                result[item.name] = item.value
            }
            
            guard let id = query[DeepLinkConstants.query] else { return .explore }
            
            return .details(reference: id)
        }
    
}

enum Tab: String {
    case explore
    case devices
    case tools
    case preferences
}

struct Orchard: Hashable, Codable, Identifiable {
    var id: Int
    var title: String
    var category: String
    var year: String
    var imageName: String
    var bannerName: String
    var URLScheme: String
    var isFavorite: Bool
    var isAR: Bool
    var ARFileName: String

    struct ProductRow: Codable, Hashable {
        let title: String
        let value: String
    }

    struct SectionHeader: Codable, Hashable {
        let title: String
        let symbol: String
    }

    struct Section: Codable, Hashable {
        let header: SectionHeader
        let rows: [ProductRow]
    }

    let sections: [Section]
    
    var favoriteString: String {
        String("\(isFavorite ? "Yes" : "No")")
       }
}

struct Catergory: Hashable, Codable, Identifiable {
    var id: Int
    var title: String
    var imageName: String
    var URLScheme: String
}
