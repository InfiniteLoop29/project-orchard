//
//  AboutAppView.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-08-03.
//

import SwiftUI
#if os(macOS)
struct AboutAppView: View {
    var body: some View {
        ZStack {
            VisualEffectView()
            VStack {
                Image("AppIconAbout").resizable().frame(width: 80, height: 80)
                Text("Orchard Pro")
                    .font(.largeTitle)
                Text("Version 0.1.0 (102)")
                    .foregroundColor(.secondary)
                    .font(.system(.body, design: .monospaced))
                Link("Orchard Pro website...", destination: URL(string: "https://www.mylink.com")!).underline()
                
                Divider().padding(EdgeInsets(top: 8, leading: 20, bottom: 8, trailing: 20))
                Text("Designed and created with love by Drake Jordan in Canada. Thank you for using and supporting Orchard Pro!").padding(.horizontal, 8).multilineTextAlignment(.center)
                    .foregroundColor(.secondary)
                    .font(.callout)
            }
        }.frame(width: 400, height: 260)
    }
}
#endif

struct AboutApp: View {
    @State private var showingDebugSheet = false
    @State var internalCount = 0
    @Environment(\.openURL) var openURL
    var body: some View {
        List {
            Section {
                VStack {
                    Button {
                        withAnimation {
                            internalCount += 1
                        }
                    } label: {
                        Image("AppIconAbout").resizable().frame(width: 80, height: 80)
                    }.buttonStyle(.plain)
                    Text("Orchard Pro")
                        .font(.largeTitle)
                    Text("Version 0.1.0 (102)")
                        .foregroundColor(.secondary)
                        .font(.system(.body, design: .monospaced))
                    Link("Orchard Pro website...", destination: URL(string: "https://www.mylink.com")!).underline().padding(.top, 0.5)
                    
                    Divider().padding(EdgeInsets(top: 8, leading: 20, bottom: 8, trailing: 20))
                    Text("Designed and created with love by Drake Jordan in Canada. Thank you for using and supporting Orchard Pro!").padding(.horizontal, 8).multilineTextAlignment(.center)
                        .foregroundColor(.secondary)
                        .font(.callout)
                }.padding(.vertical)
            }
            
            if internalCount == 5 {
                Section {
                    Button {
                        showingDebugSheet.toggle()
                    } label: {
                        Label("Debug Settings", systemImage: "ant")
                    }
                    Button {
                        openURL(URL(string: "rdar://orchard_pro")!)
                    } label: {
                        Label("File a Radar", systemImage: "ant.circle")
                    }
                } header: {
                    Text("Internal")
                }.transition(.slide)
            }
        }  .sheet(isPresented: $showingDebugSheet) {
            DebugView()
          }
    }
}
struct AboutAppView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            AboutApp()
                .navigationTitle("About")
        }
    }
}
