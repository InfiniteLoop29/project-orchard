//
//  VisualEffectView+NSViewRepresentable.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-09-10.
//

import SwiftUI
#if os(macOS)
struct VisualEffectView: NSViewRepresentable {
    func makeNSView(context: Context) -> NSVisualEffectView {
        let view = NSVisualEffectView()

        view.blendingMode = .behindWindow
        view.state = .active
        view.material = .underWindowBackground

        //.underWindowBackground
        
        return view
    }

    func updateNSView(_ nsView: NSVisualEffectView, context: Context) {
        //
    }
}
#endif
