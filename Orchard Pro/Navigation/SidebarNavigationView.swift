//
//  CatalystView.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-11-14.
//

import SwiftUI

extension TabModel {
    static var tabData: [TabModel] {
        [
            .init(name: "Explore", details: "1000", symbol: "rectangle.3.group"),
            .init(name: "Browse", details: "1000", symbol: "list.bullet.below.rectangle"),
            .init(name: "Tools", details: "1000", symbol: "wrench.and.screwdriver.fill"),
            .init(name: "iPhone", details: "0", symbol: "iphone"),
            .init(name: "iPad", details: "1", symbol: "ipad"),
            .init(name: "Watch", details: "2", symbol: "applewatch"),
            .init(name: "MacBook", details: "3", symbol: "laptopcomputer"),
            .init(name: "iMac", details: "4", symbol: "desktopcomputer"),
            .init(name: "Desktop", details: "5", symbol: "macpro.gen3"),
            .init(name: "AirPods", details: "6", symbol: "airpodspro"),
            .init(name: "HomePod", details: "7", symbol: "homepodmini"),
            .init(name: "TV", details: "8", symbol: "appletv"),
            .init(name: "Accessory", details: "9", symbol: "magicmouse"),
            .init(name: "Display", details: "10", symbol: "display"),
            .init(name: "iPod", details: "11", symbol: "ipod"),
            .init(name: "Server", details: "12", symbol: "xserve"),
        ]
    }
}

struct SidebarNavigationView: View {
    @StateObject private var modelData = ModelData()
    @State private var tabs = TabModel.tabData
    @State private var selection: TabModel?
    
    var body: some View {
        NavigationSplitView {
            List(selection: $selection) {
                ForEach(tabs) { tab in
                    if Int(tab.details)! > 100 {
                        Label(tab.name, systemImage: tab.symbol).tag(tab)
                    }
                }
                Section(header: Text("Categories")) {
                    ForEach(tabs) { tab in
                        if tab.details != "1000" {
                            Label(tab.name, systemImage: tab.symbol).tag(tab)
                        }
                    }
                }

            }.listStyle(.sidebar)
            #if os(iOS)
                .navigationTitle("Orchard Pro")
            #endif
        } detail: {
            if selection?.details != "1000" {
                NavigationStack {
                    CategoryDetailView(categoryName: selection?.name ?? "iPhone", catergory: modelData.catergoryData[Int(selection?.details ?? "0")!])
                }
            } else if selection?.name == "Explore" {
                NavigationStack {
                    ExploreView()
                }
            } else if selection?.name == "Browse" {
                NavigationStack {
                    BrowseView()
                }.navigationTitle("Browse")
            } else if selection?.name == "Tools" {
                NavigationStack {
                    ToolsView()
                }.navigationTitle("Tools")
            } else {
                NavigationStack {
                    ExploreView()
                }
            }
        }.navigationSplitViewColumnWidth(ideal: 500)
            .onAppear {
                selection = tabDatas[0]
            }
            .onOpenURL  { url in
                if url.absoluteString.contains("explore") {
                    selection = tabDatas[0]
                } else if url.absoluteString.contains("browse") {
                    selection = tabDatas[1]
                } else if url.absoluteString.contains("tools") {
                    selection = tabDatas[2]
                } else if url.absoluteString.contains("more") {
                    selection = tabDatas[3]
                }
            }
    }
}


struct SidebarNavigationView_Previews: PreviewProvider {
    static var previews: some View {
        SidebarNavigationView()
    }
}
