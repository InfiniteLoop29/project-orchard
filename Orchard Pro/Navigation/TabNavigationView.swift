//
//  MainTabView.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-10-10.
//

import SwiftUI

struct TabNavigationView: View {
    @StateObject private var modelData = ModelData()
    @State private var selectedTab = "One"
    @State private var showingSheet = false
    @State private var showingDebugSheet = false
    @State var deviceSheetInt = 0

    @Environment(\.dismiss) var dismiss
    var body: some View {
        TabView(selection: $selectedTab) {
            NavigationView {
                ExploreView().navigationTitle("Explore")
            }
            .environmentObject(modelData)
            .tabItem {
                Label("explore_navigation_title", systemImage: "rectangle.3.group")
                    .onTapGesture {
                        selectedTab = "One"
                    }
            }

            .tag("One")

            NavigationView {
                BrowseView()
            }
            .environmentObject(modelData)
            .tabItem {
                Label("devices_navigation_title", systemImage: "list.bullet.below.rectangle")
                    .onTapGesture {
                        selectedTab = "Two"
                    }
            }

            .tag("Two")

            NavigationView {
                ToolsView().navigationTitle("Tools")
            }
            .tabItem {
                Label("Tools", systemImage: "wrench.and.screwdriver.fill")
                    .onTapGesture {
                        selectedTab = "Three"
                    }
            }

            .tag("Three")
            NavigationView {
                MoreView().navigationTitle("More")
            }
            .tabItem {
                Label("More", systemImage: "ellipsis")
                    .onTapGesture {
                        selectedTab = "Four"
                    }
            }

            .tag("Four")
        }
        .sheet(isPresented: $showingSheet) {
            SheetView(deviceSheetInt: $deviceSheetInt)
                .presentationDetents([.large, .fraction(30)])
        }
        .sheet(isPresented: $showingDebugSheet) {
          DebugView()
        }
        .toolbar {
            ToolbarItem(placement: .primaryAction) {
                Button("Close") {
                    dismiss()
                }
            }
        }
        .onOpenURL  { url in
            if url.absoluteString.contains("explore") {
                selectedTab = "One"
            } else if url.absoluteString.contains("browse") {
                selectedTab = "Two"
            } else if url.absoluteString.contains("tools") {
                selectedTab = "Three"
            } else if url.absoluteString.contains("more") {
                selectedTab = "Four"
            } else if url.absoluteString.contains("orchard://device/") {
                if let number = Int(String("\(url)").components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
                    deviceSheetInt = number
                }
                showingSheet.toggle()
            } else if String("\(url)") == "orchard://debug" {
                showingDebugSheet.toggle()
            }
        }
    }
}


struct SheetView: View {
    @Environment(\.dismiss) var dismiss
    @Binding var deviceSheetInt: Int
    var body: some View {
            DeviceDetailView(orchard: ModelData().orchardData[deviceSheetInt])
    }
}

struct DebugView: View {
    @Environment(\.dismiss) var dismiss
    var body: some View {
        NavigationStack {
            List {
                Section {
                    Image(systemName: "exclamationmark.square.fill").font(.system(size: 50)).foregroundColor(.red).padding(4).symbolRenderingMode(.hierarchical)
                    Text("Careful! These settings could alter Orchard Pro in ways that make it inoperable. Only poke around here if you know what you're doing.").multilineTextAlignment(.center)
                }.padding()
                Spacer()
                Button {
                    dismiss()
                } label: {
                    HStack {
                        Spacer()
                        Text("Dismiss")
                        Spacer()
                    }.frame(height: 40)
                }.buttonStyle(.borderedProminent).padding()
            }
            .navigationTitle("Debug")
            #if os(iOS)
            .background(Color(.systemGroupedBackground))
            #endif
        }
    }
}
