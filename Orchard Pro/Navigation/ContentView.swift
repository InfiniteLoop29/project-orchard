//
//  ContentView.swift
//  Orchard
//
//  Created  by Drake Jordan on 2021-09-25.
//

import SwiftUI

struct ContentView: View {
    @AppStorage("isShowingSheet") var isFirstLaunch: Bool = true
    @StateObject private var modelData = ModelData()
    #if os(iOS)
        @Environment(\.horizontalSizeClass) private var horizontalSizeClass
    #endif

    var body: some View {
        #if os(iOS)
            if horizontalSizeClass == .compact {
                TabNavigationView()
                    .environmentObject(modelData)
            } else {
                SidebarNavigationView()
                    .environmentObject(modelData)
            }
        #else
            SidebarNavigationView()
                .environmentObject(modelData)
        #endif
    }

    struct ContentView_Previews: PreviewProvider {
        static var previews: some View {
            ContentView()
                .preferredColorScheme(.light)
                .previewInterfaceOrientation(.portrait)
        }
    }
}

