//
//  IconsView.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-08-30.
//

import SwiftUI

struct IconsView: View {
    @EnvironmentObject var modelData: ModelData
    @State var queryString = ""
    @State var showFavoritesOnly = false
    @State var sortByYearDecending = true
    @Binding var currentDevicesTab: Int
    @Binding var selectedToggle: Int
    var filteredDevices: [Orchard] {
        modelData.orchardData.sorted(by: {sortByYearDecending ? $0.year > $1.year : $0.year < $1.year}).filter { orchard in
            if !queryString.isEmpty {
                return orchard.title.localizedCaseInsensitiveContains(queryString)
            } else if showFavoritesOnly {
                return orchard.isFavorite

            } else {
                return true
            }
        }
    }
    #if os(macOS)
    let columns = [
           GridItem(.adaptive(minimum: 120))
       ]
    #else
    let columns = [
           GridItem(.adaptive(minimum: 140))
       ]
    #endif
    var body: some View {
#if os(macOS)
        List {
    Picker("", selection: $currentDevicesTab) {
        Text("devices_list_tab_title").tag(0)
        Text("Tags").tag(1)
    }.padding(EdgeInsets(top: 0, leading: 0, bottom: 10, trailing: 0))
        .pickerStyle(.segmented)
            VStack {
                ScrollView {
                    LazyVGrid(columns: columns, spacing: 15) {
                        ForEach(filteredDevices) { orchard in
                            NavigationLink(destination: DeviceDetailView(orchard: orchard)
                             ) {
                                BrowseIcon(image: orchard.imageName, title: orchard.title, year: orchard.year, isFavorite: orchard.isFavorite)
                                }.buttonStyle(.plain)
                        }
                    }
                    .padding(.vertical)
                }.background(.background)
            }
        }
        .modifier(BrowseToolbar(currentDevicesTab: $currentDevicesTab, showFavoritesOnly: $showFavoritesOnly, sortByYearDecending: $sortByYearDecending, selectedToggle: $selectedToggle, queryString: $queryString))
    #else
            VStack {
                ScrollView {
                    LazyVGrid(columns: columns, spacing: 15) {
                        ForEach(filteredDevices) { orchard in
                            NavigationLink(destination: DeviceDetailView(orchard: orchard)
                               ) {
                                BrowseIcon(image: orchard.imageName, title: orchard.title, year: orchard.year, isFavorite: orchard.isFavorite)
                                }.buttonStyle(.plain)
                        }
                    }
                    .padding(.vertical)
                }.background(.background)
            }
            .modifier(BrowseToolbar(currentDevicesTab: $currentDevicesTab, showFavoritesOnly: $showFavoritesOnly, sortByYearDecending: $sortByYearDecending, selectedToggle: $selectedToggle, queryString: $queryString))
        #endif
    }
}
