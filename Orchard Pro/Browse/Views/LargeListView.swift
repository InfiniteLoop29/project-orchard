//
//  LargeListView.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-08-30.
//

import SwiftUI

struct LargeListView: View {
    @EnvironmentObject var modelData: ModelData
    @State var queryString = ""
    @State var showFavoritesOnly = false
    @State var sortByYearDecending = true
    @Binding var currentDevicesTab: Int
    @Binding var selectedToggle: Int
    var filteredDevices: [Orchard] {
        modelData.orchardData.sorted(by: {sortByYearDecending ? $0.year > $1.year : $0.year < $1.year}).filter { orchard in
            if !queryString.isEmpty {
                return orchard.title.localizedCaseInsensitiveContains(queryString)
            } else if showFavoritesOnly {
                return orchard.isFavorite

            } else {
                return true
            }
        }
    }
    var body: some View {
        VStack {
            List {
                #if os(macOS)
                    Picker("", selection: $currentDevicesTab) {
                        Text("devices_list_tab_title").tag(0)
                        Text("Tags").tag(1)
                    }.padding(EdgeInsets(top: 0, leading: 0, bottom: 10, trailing: 0))
                        .pickerStyle(.segmented)
                #endif
                ForEach(filteredDevices) { orchard in
                    #if os(macOS)
                        GroupBox {
                            NavigationLink(destination: DeviceDetailView(orchard: orchard)
                                ) {
                                    BrowseLargeRow(orchard: orchard)
                                }
                        }
                    #else
                        NavigationLink(destination: DeviceDetailView(orchard: orchard)
                         ) {
                                BrowseLargeRow(orchard: orchard)
                            }

                            .swipeActions {
                                Button(action: { print("Swipe Action Detected") }) {
                                    Image(systemName: "star.fill")
                                }
                                .tint(.yellow)
                            }
                    #endif
                }
            }
            .modifier(BrowseToolbar(currentDevicesTab: $currentDevicesTab, showFavoritesOnly: $showFavoritesOnly, sortByYearDecending: $sortByYearDecending, selectedToggle: $selectedToggle, queryString: $queryString))
        }
    }
}
