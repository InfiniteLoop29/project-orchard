//
//  SmallListView.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-08-30.
//

import SwiftUI

struct SmallListView: View {
    @EnvironmentObject var modelData: ModelData
    @State var queryString = ""
    @State var showFavoritesOnly = false
    @State var sortByYearDecending = true
    @Binding var currentDevicesTab: Int
    @Binding var selectedToggle: Int
    var filteredDevices: [Orchard] {
        modelData.orchardData.sorted(by: {sortByYearDecending ? $0.year > $1.year : $0.year < $1.year}).filter { orchard in
            if !queryString.isEmpty {
                return orchard.title.localizedCaseInsensitiveContains(queryString)
            } else if showFavoritesOnly {
                return orchard.isFavorite

            } else {
                return true
            }
        }
    }
    var body: some View {
   Text("Small List")
            .modifier(BrowseToolbar(currentDevicesTab: $currentDevicesTab, showFavoritesOnly: $showFavoritesOnly, sortByYearDecending: $sortByYearDecending, selectedToggle: $selectedToggle, queryString: $queryString))
    }
}
