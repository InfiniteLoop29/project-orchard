//
//  TagsView.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-08-30.
//

import SwiftUI

struct TagsView: View {
    @Binding var currentDevicesTab: Int
    @State private var queryString = ""
    var body: some View {
        VStack {
            List {
                #if os(macOS)
                Picker("", selection: $currentDevicesTab) {
                    Text("devices_list_tab_title").tag(0)
                    Text("Tags").tag(1)
                }.padding(.bottom, 10)
                    .pickerStyle(.segmented)
                #endif
                HStack {
                    Spacer()
                    GroupBox {
                        Image(systemName: "exclamationmark.triangle.fill")
                            .font(Font.system(size: 80))
                        Text("This view is currently under construction")
                            .fontWeight(.semibold)
                            .padding(.top)
                            .multilineTextAlignment(.center)
                    }
                    .frame(width: 200, height: 200)
                    .foregroundColor(.secondary)
                    .padding()
                    Spacer()
                }
            }
        }
        #if os(macOS)
        .searchable(text: $queryString, prompt: "Search tags...")
        #endif

        #if os(iOS)
        .searchable(text: $queryString, placement: .navigationBarDrawer(displayMode: .always), prompt: "Search tags...")
        .navigationBarTitle("Tags")
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Image(systemName: "plus.circle")
                    .foregroundColor(.accentColor)
            }

            ToolbarItem(placement: .navigationBarLeading) {
                Picker("devices_tab_hidden_text", selection: $currentDevicesTab) {
                    Text("devices_list_tab_title").tag(0)
                    Text("Tags").tag(1)
                }
                .pickerStyle(.segmented)
            }
        }
        #endif
        #if os(iOS)
        .background(Color(.systemGroupedBackground))
        #endif
    }
}
