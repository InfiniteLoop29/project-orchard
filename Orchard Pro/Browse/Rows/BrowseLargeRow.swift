//
//  OrchardRow.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-10-03.
//

import SwiftUI

struct BrowseLargeRow: View {
    var orchard: Orchard

    var body: some View {
        HStack {
            Image(orchard.imageName)
                .resizable()
            #if os(iOS)
                .frame(width: 40, height: 40)
            #else
                .frame(width: 30, height: 30)
            #endif
            VStack(alignment: .leading) {
                Text(orchard.title)
#if os(iOS)
                    .font(.system(size: 20))
#else
                    .font(.system(size: 15))
#endif
                   
                    .fontWeight(.regular)

                Text(orchard.year)
#if os(iOS)
                    .font(.callout)
#else
                    .font(.footnote)
#endif
                   
                    .foregroundColor(.secondary)
            }
            .padding(.leading)

            Spacer()
            
            if orchard.isFavorite {
                Image(systemName: "star.fill")
                    .foregroundColor(.yellow)
            }
            #if os(macOS)
            Image(systemName: "chevron.right")
                .padding(.trailing, 4)
                .imageScale(.small)
                .foregroundColor(.secondary)
            #endif
        }
    }
}

struct BrowseLargeRow_Previews: PreviewProvider {
    static var orchardData = ModelData().orchardData

    static var previews: some View {
        BrowseLargeRow(orchard: ModelData().orchardData[0])
            .environmentObject(ModelData())
           
    }
}
