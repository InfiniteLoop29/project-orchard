//
//  SwiftUIView.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-08-28.
//

import SwiftUI

struct BrowseIcon: View {
var image: String
    var title: String
    var year: String
    var isFavorite: Bool
    @State private var isHovering = false
    var body: some View {
            VStack(alignment: .center) {
                Image(image)
                    .resizable()
                    .frame(width: 80, height: 80)
                Text(title)
                    .font(.callout)
                    .multilineTextAlignment(.center)
                if !isFavorite {
                    Text(year)
                        .font(.footnote)
                        .foregroundColor(.secondary)
                        .multilineTextAlignment(.center)
                }else {
                    HStack {
                        Text(year)
                        Text("•")
                        Image(systemName: "star.fill").imageScale(.small).foregroundColor(.yellow)
                    }
                        .font(.footnote)
                        .foregroundColor(.secondary)
                        .multilineTextAlignment(.center)
                }
            }.frame(width: 100)
        #if os(macOS)
            .padding(10)
            .background {
                if isHovering {
                    RoundedRectangle(cornerRadius: 10).foregroundColor(Color("GroupBoxColor"))
                }
            }
        #endif
                .onHover { hover in
                    isHovering = hover
                }
    }
}

struct BrowseIcon_Previews: PreviewProvider {
    static var previews: some View {
        BrowseIcon(image: "apple_tv_1", title: "zsjd", year: "2021", isFavorite: true)
    }
}
