//
//  ListView.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-10-03.
//

import Combine
import SwiftUI

struct BrowseView: View {
    @State private var currentDevicesTab = 0
    @AppStorage("selectedToggle") var selectedToggle = 0

    var body: some View {
        if currentDevicesTab == 0 {
            if selectedToggle == 0 {
                LargeListView(currentDevicesTab: $currentDevicesTab, selectedToggle: $selectedToggle)
            } else if selectedToggle == 2 {
                SmallListView(currentDevicesTab: $currentDevicesTab, selectedToggle: $selectedToggle)
            } else {
                IconsView(currentDevicesTab: $currentDevicesTab, selectedToggle: $selectedToggle)
            }
        } else {
            TagsView(currentDevicesTab: $currentDevicesTab)
        }
    }
}

struct BrowseView_Previews: PreviewProvider {
    static let modelData = ModelData()

    static var previews: some View {
        NavigationStack {
            BrowseView()
        }
                .environmentObject(ModelData())
        
    }
}

