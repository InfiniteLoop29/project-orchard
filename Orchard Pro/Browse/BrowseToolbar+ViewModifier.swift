//
//  SwiftUIView.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-08-30.
//

import SwiftUI

struct BrowseToolbar: ViewModifier {
    @Binding var currentDevicesTab: Int
    @Binding var showFavoritesOnly: Bool
    @Binding var sortByYearDecending: Bool
    func toggleBinding(for num: Int) -> Binding<Bool> {
        Binding {
            num == selectedToggle
        } set: { _ in
            selectedToggle = num
        }
    }
    @Binding var selectedToggle: Int
    var toggleNames = [0: "as List",
                            1: "as Icons",
                            2: "as Gallery"]
    var toggleSymbols = [0: "list.bullet",
                            1: "square.grid.2x2",
                            2: "squares.below.rectangle"]
    @Binding var queryString: String
    func body(content: Content) -> some View {
        content
#if os(macOS)
        .searchable(text: $queryString, prompt: "devices_search_query_title")
        #else
        .searchable(text: $queryString, placement: .navigationBarDrawer(displayMode: .always), prompt: "devices_search_query_title")
        #endif
        
        
#if os(iOS)
.navigationBarTitle("devices_navigation_title", displayMode: .large)

#endif
        .toolbar {
            ToolbarItem(placement: .automatic) {
                Menu {
                    Section {
                        ForEach(0...2, id: \.self) { num in
                            Toggle(" \(toggleNames[Int(num)]!)", isOn: toggleBinding(for: num))
                        }
                    } header: {
                        Text("Show")
                    }
                    Section {
                        Toggle(isOn: $sortByYearDecending) {
                            Text("by Year Descending")
                        }
                    } header: {
                        Text("Sort")
                    }
                    Section {
                        Toggle(isOn: $showFavoritesOnly) {
                            Text("devices_filter_favorites_title")
                        }
                    } header: {
                        Text("Filter")
                    }
                } label: {
                    Label(toggleSymbols[selectedToggle]!, systemImage: toggleSymbols[selectedToggle]!)
                }
            }
            #if os(iOS)
            ToolbarItem(placement: .navigationBarLeading) {
                Picker("devices_tab_hidden_text", selection: $currentDevicesTab) {
                    Text("List").tag(0)
                    Text("Tags").tag(1)
                }
                .pickerStyle(.segmented)
            }
            #endif
        }
    }
}
