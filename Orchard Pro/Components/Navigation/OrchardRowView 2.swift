//
//  OrchardRow.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-10-03.
//

import SwiftUI

struct OrchardRow: View {
    var orchard: Orchard
    
    var body: some View {
        HStack {
            Image(orchard.imageName)
                .resizable()
                .frame(width: 50, height: 50)
            Text(orchard.title)
                .font(.title3)
                .fontWeight(.regular)

            Spacer()

            if orchard.isFavourite {
                Image(systemName: "star.fill")
                    .foregroundColor(.yellow)
            }
        }
    }
}

struct OrchardRow_Previews: PreviewProvider {
    static var orchardData = ModelData().orchardData
    
    static var previews: some View {
        OrchardRow(orchard: ModelData().orchardData[0])
            .environmentObject(ModelData())
        .previewLayout(.fixed(width: 300, height: 70))
    }
}
