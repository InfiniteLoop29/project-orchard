//
//  PreferencesView.swift
//  Orchard
//  Image(systemName: "exclamationmark.circle").foregroundColor(.red)
// .font(.callout)
//  Created by Drake Jordan on 2021-12-07.
//

#if os(iOS)
    import MessageUI
#endif

import StoreKit
import SwiftUI

struct MoreView: View {
    @StateObject private var modelData = ModelData()
    @State private var showSheet = false
    @State var showingPreview = false
    @State private var showingAppVersionAlert = false
    @State private var showingMailAlert = false
    #if os(iOS)
        @State private var contactMailData = ComposeMailData(subject: "",
                                                             recipients: ["hi@drakejordan.com"],
                                                             message: "Leave a message here",
                                                             attachments: nil)
        @State private var reportMailData = ComposeMailData(subject: "Orchard Bug Report",
                                                            recipients: ["support@drakejordan.com"],
                                                            message: "Orchard Version 0.1.8 (82) DEV - Leave your bug report below",
                                                            attachments: nil)
    #endif
    @State var isShowingMailView = false
    @State var isShowingOnboardingView = false

    var body: some View {
        VStack {
            List {
                Section {
                    ChildView(destinationView: AppearanceView(), title: "Appearance", symbol: "paintpalette")
                } header: {
                    Text("Looks")
                }
                
                Section {
                    ChildView(destinationView: iCloud(), title: "iCloud", symbol: "icloud")
                } header: {
                    Text("Details")
                }
                
                Section {
                    ChildView(destinationView: MeetMeView(), title: "Meet Me", symbol: "hand.wave")
                    ChildView(destinationView: AboutApp(), title: "About", symbol: "info.square")
                } header: {
                    Text("About")
                }

            }
        }
        #if os(iOS)
            .navigationBarTitle("More", displayMode: .large)
        #endif
    }
}

struct MoreView_Previews: PreviewProvider {
    static var previews: some View {
        MoreView()
    }
}


struct ChildView<Content: View>: View {
    var destinationView: Content
    var title: String
    var symbol: String

    init(destinationView: Content,  title: String, symbol: String) {
        self.destinationView = destinationView
        self.title = title
        self.symbol = symbol
    }

    var body: some View {
        NavigationLink(destination: destinationView.navigationTitle(title)) {
            Label {
                Text(title)

            } icon: {
                Image(systemName: symbol)
                    .foregroundColor(.accentColor)
            }
        }
        }
    }


