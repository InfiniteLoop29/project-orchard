//
//  FAQView.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-10-13.
//

import SwiftUI

struct HelpView: View {
    var body: some View {
        List {
            HStack {
                Spacer()
                GroupBox {
                    Image(systemName: "exclamationmark.triangle.fill")
                        .font(Font.system(size: 80))
                    Text("This view is currently under construction")
                        .fontWeight(.semibold)
                        .padding(.top)
                        .multilineTextAlignment(.center)
                }
                .frame(width: 200, height: 200)
                .foregroundColor(.secondary)
                .padding()
                Spacer()
            }
        }
    }
}

struct BlueGroupBox: GroupBoxStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.content

            .padding()
            .background(RoundedRectangle(cornerRadius: 10).fill(Color.accentColor))
            .overlay(configuration.label.padding(.leading, 4), alignment: .topLeading)
    }
}

struct SupportView_Previews: PreviewProvider {
    static var previews: some View {
        HelpView()
    }
}

struct HelpSettingsView: View {
    var body: some View {
        Text("Help")
            .font(.title)
            .frame(width: 470, height: 550)
    }
}
