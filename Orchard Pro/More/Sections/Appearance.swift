//
//  AppearanceView.swift
//  Orchard
//
//  Created by Drake Jordan on 2022-02-04.
//

import SwiftUI



struct Item {
    let colorName: String
    let color: Color
}

struct AppearanceView: View {
    @EnvironmentObject var modelData: ModelData
    @State private var multicolorSymbolsEnabled = false
    @State var currentAccentColor = Color.blue
    let colors: [Color] = [Color.blue, Color.purple, Color.pink, Color.red, Color.orange, Color.yellow, Color.green, Color.gray]
    func getColor() -> String {
        return "\(currentAccentColor)".capitalized
    }
    var body: some View {
        VStack {
            List {
                Section {
                    VStack {
                        HStack {
                            Spacer()
                            AppearanceButton(imageName: "iOS_Light", colorSchemeNumber: 0, colorSchemeName: "Light")
                            Spacer()
                            AppearanceButton(imageName: "iOS_Dark", colorSchemeNumber: 1, colorSchemeName: "Dark")
                            Spacer()
                            AppearanceButton(imageName: "iOS_Auto", colorSchemeNumber: 2, colorSchemeName: "Auto")
                            Spacer()
                        }.font(.system(size: 15)).frame(height: 250)
                        
                    }
                } header: {
                    Text("Color Scheme")
                }
                Section {
                    ColorPicker().padding(1)
                    HStack {
                        HStack {
                            VStack {
                                Text("Multicolor symbols")
                            }
                        
                        }
                        Spacer()
                        HStack {
                            Toggle("", isOn: $multicolorSymbolsEnabled)
                                .toggleStyle(.switch)
                        }
                    }
                } header: {
                    Text("Accent Color")
                }
                }
                
            
        }
    }
}


struct AppearanceView_Previews: PreviewProvider {
    static let modelData = ModelData()
    static var previews: some View {
        NavigationStack {
            AppearanceView()
                .environmentObject(modelData)
                .preferredColorScheme(.light)
                .navigationTitle("Appearance")
        }
    }
}

struct ColorPicker: View {
    @State var currentAccentColor = Color.blue
    let colors: [Color] = [Color.blue, Color.purple, Color.pink, Color.red, Color.orange, Color.yellow, Color.green, Color.gray]
    func getColor() -> String {
        return "\(currentAccentColor)".capitalized
    }
    var body: some View {
        VStack {
            HStack {
                HStack {
                    VStack {
                        HStack {
                            VStack {
                                HStack {
                                    ForEach(colors, id: \.self) { color in
                                        Button {
                                            currentAccentColor = color
                                        } label: {
                                            VStack {
                                                ZStack {
                                                    Circle()
                                                        .fill(color)
                                                        .frame(width: 28, height: 28)
                                                    Circle()
                                                        .stroke(.primary, lineWidth: 1.5)
                                                        .frame(width: 27, height: 27)
                                                        .opacity(0.2)
                                                    if currentAccentColor == color {
                                                        Circle()
                                                            .foregroundColor(.white)
                                                            .frame(width: 10, height: 10)
                                                    }
                                                }
                                            }
                                        }.buttonStyle(.plain).padding(.horizontal, 2)
                                    }
                                    Spacer()
                                }
                                HStack {
                                   
                                    Text(getColor())
                                        .font(.callout)
                                        .foregroundColor(.secondary)
                                    Spacer()
                                }
                            }
                        }
                    }.frame(height: 60)
                }
            }
        }
    }
}

struct AppearanceButton: View {
    @EnvironmentObject var modelData: ModelData
    var imageName: String
    var colorSchemeNumber: Int
    var colorSchemeName: String
    var body: some View {
        Button {
            modelData.appSettings.colorScheme = colorSchemeNumber
        } label: {
            VStack {
                ZStack {
                    Image(imageName)
                        .resizable()
                        .frame(width: 80, height: 160)
                }
                HStack {
                    VStack {
                            Text(colorSchemeName)
                             
                        if  modelData.appSettings.colorScheme == colorSchemeNumber {
                            Image(systemName: "checkmark.circle.fill").foregroundColor(.accentColor).imageScale(.large).padding(0.5)
                        } else {
                            Image(systemName: "circle").foregroundColor(.secondary).imageScale(.large).padding(0.5)
                        }
                    }
                }
            }.padding(.top, 6)
        }.buttonStyle(.plain).padding(.horizontal, 2).frame(width: 80, height: 58)
    }
}

struct AppearanceSettingsView: View {
    @State private var multicolorSymbolsEnabled = false
    @State var currentAppearance = 2
    @State var currentAccentColor = Color.blue
    let colors: [Color] = [Color.blue, Color.purple, Color.pink, Color.red, Color.orange, Color.yellow, Color.green, Color.gray]
    func getColor() -> String {
        return "\(currentAccentColor)".capitalized
    }

    var body: some View {
        VStack {
            GroupBox {
                VStack {
                    HStack {
                        HStack {
                            VStack {
                                HStack {
                                    Text("Appearance")
                                        .padding(EdgeInsets(top: 4, leading: 4, bottom: 0, trailing: 0))
                                    Spacer()
                                }
                                Spacer()
                            }
                        }
                        Spacer()
                        HStack {
                            Button {
                                currentAppearance = 0
                            } label: {
                                VStack {
                                    ZStack {
                                        Image("macOS_Light")
                                            .resizable()
                                            .frame(width: 68, height: 46)
                                        if currentAppearance == 0 {
                                            RoundedRectangle(cornerRadius: 6)
                                                .stroke(.blue, lineWidth: 3)
                                                .frame(width: 73, height: 51)
                                        }
                                    }
                                    HStack {
                                        if currentAppearance == 0 {
                                            Text("Light")
                                                .fontWeight(.semibold)
                                        } else {
                                            Text("Light")
                                                .foregroundColor(.secondary)
                                        }
                                    }
                                }.padding(.top, 6)
                            }.buttonStyle(.plain).padding(.horizontal, 2).frame(width: 80, height: 58)
                            Button {
                                currentAppearance = 1
                            } label: {
                                VStack {
                                    ZStack {
                                        Image("macOS_Dark")
                                            .resizable()
                                            .frame(width: 68, height: 46)
                                        if currentAppearance == 1 {
                                            RoundedRectangle(cornerRadius: 6)
                                                .stroke(.blue, lineWidth: 3)
                                                .frame(width: 73, height: 51)
                                        }
                                    }
                                    HStack {
                                        if currentAppearance == 1 {
                                            Text("Dark")
                                                .fontWeight(.semibold)
                                        } else {
                                            Text("Dark")
                                                .foregroundColor(.secondary)
                                        }
                                    }
                                }.padding(.top, 6)
                            }.buttonStyle(.plain).padding(.horizontal, 2).frame(width: 80, height: 58)
                            Button {
                                currentAppearance = 2
                            } label: {
                                VStack {
                                    ZStack {
                                        Image("macOS_Auto")
                                            .resizable()
                                            .frame(width: 68, height: 46)
                                        if currentAppearance == 2 {
                                            RoundedRectangle(cornerRadius: 6)
                                                .stroke(.blue, lineWidth: 3)
                                                .frame(width: 73, height: 51)
                                        }
                                    }
                                    HStack {
                                        if currentAppearance == 2 {
                                            Text("Auto")
                                                .fontWeight(.semibold)
                                        } else {
                                            Text("Auto")
                                                .foregroundColor(.secondary)
                                        }
                                    }
                                }.padding(.top, 6)
                            }.buttonStyle(.plain).padding(.horizontal, 2).frame(width: 78, height: 56)
                        }.padding(6).font(.system(size: 11))
                    }
                }.frame(height: 80)

                Divider()
                    .padding(.horizontal, 4)

                VStack {
                    HStack {
                        HStack {
                            VStack {
                                HStack {
                                    HStack {
                                        VStack {
                                            HStack {
                                                Text("Accent Color")
                                                    .padding(EdgeInsets(top: 8, leading: 4, bottom: 0, trailing: 0))
                                                Spacer()
                                            }
                                            Spacer()
                                        }
                                    }
                                    Spacer()
                                    VStack {
                                        HStack {
                                            ForEach(colors, id: \.self) { color in
                                                Button {
                                                    currentAccentColor = color
                                                } label: {
                                                    VStack {
                                                        ZStack {
                                                            Circle()
                                                                .fill(color)
                                                                .frame(width: 16, height: 16)
                                                            Circle()
                                                                .stroke(.primary, lineWidth: 1.5)
                                                                .frame(width: 15, height: 15)
                                                                .opacity(0.2)
                                                            if currentAccentColor == color {
                                                                Circle()
                                                                    .foregroundColor(.white)
                                                                    .frame(width: 6, height: 6)
                                                            }
                                                        }
                                                    }
                                                }.buttonStyle(.plain).padding(.horizontal, 2)
                                            }
                                        }
                                        HStack {
                                            Text(getColor())
                                                .font(.callout)
                                                .foregroundColor(.secondary)
                                            Spacer()
                                        }.padding(.leading, -2)
                                    }
                                }
                            }.frame(height: 60)
                        }
                        Spacer()
                        HStack {
                        }
                    }
                }

                Divider()
                    .padding(.horizontal, 4)

                VStack {
                    HStack {
                        HStack {
                            VStack {
                                Text("Multicolor symbols")
                            }
                            Spacer()
                        }
                        Spacer()
                        HStack {
                            Toggle("", isOn: $multicolorSymbolsEnabled)
                                .toggleStyle(.switch)
                                .scaleEffect(x: 0.8, y: 0.8)
                        }
                    }
                }.padding(4).frame(height: 30)
            }.padding()
        }.frame(width: 470, height: 240)
    }
}

