//
//  MeetMeView.swift
//  Orchard
//
//  Created by Drake Jordan on 2021-10-13.
//

import SwiftUI

struct MeetMeView: View {
    var body: some View {
        ScrollView {

            Image("meet_me_image")
                .resizable()
                .clipShape(Circle())
                .shadow(radius: 25, x: 0, y: 4)
                .frame(width: 110, height: 110)
            
            Text("meet_title")
                .font(.largeTitle)
                .fontWeight(.medium)
                .offset(x: 0, y: 10)

            Divider()
                .padding()
            GroupBox {
                Text("meet_text")
                    .font(.title3)
                    .redacted(reason: .placeholder)
            }
            .groupBoxStyle(TransparentGroupBox())
            .padding()

            GroupBox {
                Link("Visit my website", destination: URL(string: "https://www.drakejordan.com")!)
                    .font(.title3)
            }
            .groupBoxStyle(TransparentGroupBox())
            .padding()
            Spacer()
        }
        #if os(iOS)
            .background(Color(.systemGroupedBackground))
        #endif
    }
}

struct TransparentGroupBox: GroupBoxStyle {
    @Environment(\.colorScheme) var colorScheme
    func makeBody(configuration: Configuration) -> some View {
        configuration.content
            .frame(maxWidth: .infinity)
            .padding()
        #if os(iOS)
            .background(RoundedRectangle(cornerRadius: 8).fill(colorScheme == .dark ? Color(.systemGray6) : .white))
        #else
            .background(RoundedRectangle(cornerRadius: 8).fill(Color("GroupBoxColor")))
        #endif
        .overlay(configuration.label.padding(.leading, 4), alignment: .topLeading)
    }
}



struct MeetMe: View {
    @Binding var debugTaps: Int
    var body: some View {
        VStack {
            Rectangle().opacity(0).frame(height: 15)
            Image("meet_me_image")
                .resizable()
                .clipShape(Circle())
                .shadow(radius: 25, x: 0, y: 4)
                .frame(width: 110, height: 110)
                .onTapGesture {
                    debugTaps += 1
                }

            
            Text("meet_title")
                .font(.largeTitle)
                .fontWeight(.medium)
            
            Divider()
                .padding()
            GroupBox {
                Text("meet_text")
                    .font(.title3)
                    .redacted(reason: .placeholder)
            }.padding(.horizontal)
                Link("Visit my website", destination: URL(string: "https://www.drakejordan.com")!)
                    .font(.title3)
                    .padding(4)
                    .underline()
            
            Spacer()
        }
            .frame(width: 470, height: 380)
    }
}
