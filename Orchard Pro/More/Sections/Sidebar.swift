//
//  SidebarView.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-09-01.
//

import SwiftUI

var tabDatas: [TabModel] {
    [
        .init(name: "Explore", details: "1000", symbol: "rectangle.3.group"),
        .init(name: "Browse", details: "1000", symbol: "list.bullet.below.rectangle"),
        .init(name: "Tools", details: "1000", symbol: "wrench.and.screwdriver.fill"),
        .init(name: "iPhone", details: "0", symbol: "iphone"),
        .init(name: "iPad", details: "1", symbol: "ipad"),
        .init(name: "Watch", details: "2", symbol: "applewatch"),
        .init(name: "MacBook", details: "3", symbol: "laptopcomputer"),
        .init(name: "iMac", details: "4", symbol: "desktopcomputer"),
        .init(name: "Desktop", details: "5", symbol: "macpro.gen3"),
        .init(name: "AirPods", details: "6", symbol: "airpodspro"),
        .init(name: "HomePod", details: "7", symbol: "homepodmini"),
        .init(name: "TV", details: "8", symbol: "appletv"),
        .init(name: "Accessory", details: "9", symbol: "magicmouse"),
        .init(name: "Display", details: "10", symbol: "display"),
        .init(name: "iPod", details: "11", symbol: "ipod"),
        .init(name: "Server", details: "12", symbol: "xserve"),
    ]
}

struct SidebarView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct SidebarView_Previews: PreviewProvider {
    static var previews: some View {
        SidebarView()
    }
}
#if os(macOS)
struct SidebarSettingsView: View {
    #if os(macOS)
    struct VisualEffectView: NSViewRepresentable {
        func makeNSView(context: Context) -> NSVisualEffectView {
            let view = NSVisualEffectView()

            view.blendingMode = .behindWindow
            view.state = .active
            view.material = .underWindowBackground

            return view
        }

        func updateNSView(_ nsView: NSVisualEffectView, context: Context) {
            //
        }
    }
    #endif
    @State private var checkBox = true
    var body: some View {
        VStack {
            HStack {
                Text("Show these items in the sidebar:").padding(EdgeInsets(top: 0, leading: 25, bottom: 0, trailing: 0)).offset(y: 5)
                Spacer()
            }
            ZStack {
                RoundedRectangle(cornerRadius: 5).stroke(.quaternary, lineWidth: 2)
                    .frame(width: 440, height: 472)
                HStack {
                    EqualIconWidthDomain {
                        VStack {
                            Text("Main tabs")
                                .fontWeight(.medium)
                                .font(.callout)
                                .foregroundColor(.secondary)
                                .offset(x: -28)
                            VStack(alignment: .leading) {
                                ForEach(tabDatas) { tab in
                                    if Int(tab.details)! == 1000 {
                                        Toggle(isOn: $checkBox) {
                                            Label(tab.name, systemImage: tab.symbol)
                                        }
                                        .toggleStyle(.checkbox)
                                    }
                                }
                                Text("Categories")
                                    .fontWeight(.medium)
                                    .font(.callout)
                                    .foregroundColor(.secondary)
                                    .padding(.top, 8)
                                ForEach(tabDatas) { tab in
                                    if Int(tab.details)! < 1000 {
                                        Toggle(isOn: $checkBox) {
                                            Label(tab.name, systemImage: tab.symbol)
                                        }
                                        .toggleStyle(.checkbox)
                                    }
                                }
                            }

                        }.padding()
                        Spacer()
                    }
                }.background(VisualEffectView().ignoresSafeArea()).cornerRadius(5).padding(.horizontal)
                Spacer()
            }
        }.frame(width: 470, height: 540)
    }
}
#endif
