//
//  iCloud.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-09-01.
//

import SwiftUI

struct iCloud: View {
    @State private var multicolorSymbolsEnabled = false
    var body: some View {
        VStack {
            List {
                Section {
                    HStack {
                        VStack(alignment: .leading) {
                            Label {
                                Text("iCloud sync available (Apple)")
                                    .font(.body)
                                    .foregroundColor(.secondary)
                            } icon: {
                                Image(systemName: "circle.fill")
                                    .foregroundColor(.green)
                            }.padding(1)
                            
                            Label {
                                Text("App ready to sync (Orchard Pro)")
                                    .font(.body)
                                    .foregroundColor(.secondary)
                            } icon: {
                                Image(systemName: "circle.fill")
                                    .foregroundColor(.green)
                            }.padding(1)
                            
                            Label {
                                Text("App latest sync (Orchard Pro)")
                                    .font(.body)
                                    .foregroundColor(.secondary)
                            } icon: {
                                Image(systemName: "circle.fill")
                                    .foregroundColor(.green)
                            }.padding(1)
                        }
                        Spacer()
                    }.padding(2)
                } header: {
                    Text("iCloud Sync Status")
                } footer: {
                    Text("Which iCloud Sync services are currently available both with Orchard Pro and Apple.")
                }
                Section {
                    
                    Toggle("Sync favorites", isOn: $multicolorSymbolsEnabled)
                    Toggle("Sync app layout", isOn: $multicolorSymbolsEnabled)
                    Toggle("Download latest data", isOn: $multicolorSymbolsEnabled)
                    
                } header: {
                    Text("iCloud Sync Settings")
                } footer: {
                    Text("Choose what you want Orchard Pro to sync and download to and from iCloud.")
                }.padding(1)
                Section {
                    VStack {
                        HStack {
                            Spacer()
                            Button("Force iCloud sync") {
                                print("Button tapped!")
                            }.buttonStyle(.borderedProminent)
                            Spacer()
                        }
                        Button {
                            print("Image tapped!")
                        } label: {
                            Text("Disable automatic iCloud syncing")
                                .foregroundColor(.red)
                        }.buttonStyle(.bordered)
                        
                    }
                } header: {
                    Text("More")
                }.padding(2)
            }
        }
    }
}

struct iCloud_Previews: PreviewProvider {
    static var previews: some View {
        iCloud()
    }
}

struct iCloudSettingsView: View {
    @State private var multicolorSymbolsEnabled = false
    @State var currentAppearance = 2
    @State var currentAccentColor = Color.blue
    let colors: [Color] = [Color.blue, Color.purple, Color.pink, Color.red, Color.orange, Color.yellow, Color.green, Color.gray]
    func getColor() -> String {
        return "\(currentAccentColor)".capitalized
    }

    var body: some View {
        VStack {
            GroupBox {
                VStack {
                    HStack {
                        Text("iCloud Sync Status")
                        Spacer()
                    }.padding(EdgeInsets(top: 4, leading: 4, bottom: 0, trailing: 0))
                }
                HStack {
                    VStack(alignment: .leading) {
                        Label {
                            Text("iCloud sync available (Apple)")
                                .font(.body)
                                .foregroundColor(.secondary)
                        } icon: {
                            Image(systemName: "circle.fill")
                                .foregroundColor(.green)
                        }.padding(1)

                        Label {
                            Text("App ready to sync (Orchard Pro)")
                                .font(.body)
                                .foregroundColor(.secondary)
                        } icon: {
                            Image(systemName: "circle.fill")
                                .foregroundColor(.green)
                        }.padding(1)

                        Label {
                            Text("App latest sync (Orchard Pro)")
                                .font(.body)
                                .foregroundColor(.secondary)
                        } icon: {
                            Image(systemName: "circle.fill")
                                .foregroundColor(.green)
                        }.padding(1)
                    }
                    Spacer()
                }.padding(EdgeInsets(top: 0, leading: 4, bottom: 4, trailing: 0))
                Divider().padding(.horizontal)
                VStack {
                    HStack {
                        Text("iCloud Sync Settings")
                        Spacer()
                    }.padding(EdgeInsets(top: 4, leading: 4, bottom: 0, trailing: 0))
                    HStack {
                        VStack(alignment: .leading) {
                            Toggle("Sync favorites", isOn: $multicolorSymbolsEnabled)
                            Toggle("Sync app layout", isOn: $multicolorSymbolsEnabled)
                            Toggle("Download latest data", isOn: $multicolorSymbolsEnabled)
                        }
                        Spacer()
                    }.padding(EdgeInsets(top: 0, leading: 4, bottom: 4, trailing: 0))
                }
            }
            HStack {
                Button("Force iCloud sync") {
                    print("Button tapped!")
                }

                Button {
                    print("Image tapped!")
                } label: {
                    Text("Disable automatic iCloud syncing")
                        .foregroundColor(.red)
                }
            }
        }.frame(width: 438, height: 270).padding()
    }
}
