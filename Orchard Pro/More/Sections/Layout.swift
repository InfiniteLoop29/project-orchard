//
//  LayoutView.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-09-01.
//

import SwiftUI

struct LayoutView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct LayoutView_Previews: PreviewProvider {
    static var previews: some View {
        LayoutView()
    }
}

struct LayoutSettingsView: View {
    var body: some View {
        Text("Layout Settings")
            .font(.title)
            .frame(width: 470, height: 550)
    }
}
