//
//  SettingsView.swift
//  Orchard Pro
//
//  Created by Drake Jordan on 2022-07-24.
//

import SwiftUI

#if os(macOS)
    struct SettingsView: View {
        @State private var showingDebugSheet = false
        @State var debugTaps = 0
        @State var tabSelection = 0
        var body: some View {
            TabView(selection: $tabSelection) {
                LayoutSettingsView()
                    .tag(0)
                    .tabItem {
                        Label("Layout", systemImage: "rectangle.3.group")
                    }
                SidebarSettingsView()
                    .tag(1)
                    .tabItem {
                        Label("Sidebar", systemImage: "sidebar.left")
                    }

                AppearanceSettingsView()
                    .tag(2)
                    .tabItem {
                        Label("Appearance", systemImage: "paintpalette")
                    }

                iCloudSettingsView()
                    .tag(3)
                    .tabItem {
                        Label("iCloud", systemImage: "icloud")
                    }

                HelpSettingsView()
                    .tag(4)
                    .tabItem {
                        Label("Help", systemImage: "questionmark.circle")
                    }

                MeetMe(debugTaps: $debugTaps)
                    .transition(.slide)
                    .tag(5)
                    .tabItem {
                        Label("Meet Me", systemImage: "hand.wave")
                    }
                if debugTaps == 5 {
                    DebugView()
                        .transition(.slide)
                        .tag(6)
                        .tabItem {
                            Label("Debug", systemImage: "ant")
                        }
                }
            }
            .sheet(isPresented: $showingDebugSheet) {
              DebugView()
            }
        }
    }

    struct SettingsView_Previews: PreviewProvider {
        static var previews: some View {
            SettingsView()
        }
    }

 

    
#endif
