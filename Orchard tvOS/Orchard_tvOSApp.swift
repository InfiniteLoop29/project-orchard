//
//  Orchard_tvOSApp.swift
//  Orchard tvOS
//
//  Created by Drake Jordan on 2022-03-30.
//

import SwiftUI

@main
struct Orchard_tvOSApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
