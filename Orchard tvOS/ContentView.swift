//
//  ContentView.swift
//  Orchard tvOS
//
//  Created by Drake Jordan on 2022-03-30.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
